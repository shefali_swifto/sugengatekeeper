import 'dart:convert';

import 'package:sugengatekeeper/loginScreen.dart';
import 'package:sugengatekeeper/screens/canteen/bookingList.dart';
import 'package:sugengatekeeper/screens/complain/userComplaintScreen.dart';
import 'package:sugengatekeeper/screens/movietheater/bookingListScreen.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:sugengatekeeper/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:sugengatekeeper/screens/visitor/visitorScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  changeScreen() async {
    String user = await KeyConstant.retriveString(KeyConstant.key_user);
    if (user.isNotEmpty && user.contains('role_text')) {
      Map resUser = json.decode(user);
      Constant.showLog(resUser);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) {
            return (resUser['role_text'] == 'security_guard')
                ? const VisitorScreen()
                : (resUser['role_text'] == 'technician')
                    ? const UserComplaintScreen()
                    : (resUser['role_text'] == KeyConstant.role_show_checker)
                        ? const BookingListScreen()
                        : (resUser['role_text']==KeyConstant.role_canteen_admin)
                ? const BookingList()
                :Container();
          },
        ),
      );
    } else {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) {
            return LoginScreen();
          },
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(new Duration(milliseconds: 10), () {
      changeScreen();
    });
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: defaultClr,
        body: Center(
          child: Image.asset(
            "assets/logo.png",
            height: 250,
            width: 250,
            alignment: Alignment.center,
          ),
        ));
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {}
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
