import 'package:flutter/material.dart';

const defaultClr = Colors.blueGrey;
const btnTextClr = Colors.white;

const acknowledgeClr = Colors.blue;
const pendingClr = Colors.blue;
const solvedClr = Colors.green;
Color completeClr = Colors.green.shade800;
const cancelClr = Colors.red;
Color closedClr = Colors.deepOrange;

ThemeData buildLightTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
      brightness: Brightness.light,
      colorScheme: base.colorScheme.copyWith(secondary: Colors.black),
      primaryColorLight: Colors.white54,
      hintColor: Colors.grey,
      primaryColor: Colors.white,
      textSelectionTheme: const TextSelectionThemeData(
        cursorColor: Colors.black,
        selectionColor: defaultClr,
      ),
      dividerColor: Colors.grey,
      scaffoldBackgroundColor: Colors.white,
      appBarTheme: const AppBarTheme(
        elevation: 0,
        color: Colors.blueGrey,
        toolbarTextStyle: TextStyle(
            color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal),
        titleTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      cardColor: const Color(0XFFF5F5F5),
      buttonTheme: ButtonThemeData(
          height: 50,
          buttonColor: defaultClr,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          textTheme: ButtonTextTheme.primary),
      iconTheme: const IconThemeData(color: Colors.black));
}

ThemeData buildDarkTheme() {
  final ThemeData base = ThemeData.dark();
  return base.copyWith(
    colorScheme: base.colorScheme.copyWith(secondary: Colors.white),
    brightness: Brightness.dark,
    primaryColor: Colors.black,
    primaryColorLight: Colors.black45,
    scaffoldBackgroundColor: Colors.black,
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: Colors.white,
      selectionColor: defaultClr,
      // selectionHandleColor: Colors.blue,
    ),
    hintColor: Colors.white,
    dividerColor: const Color(0XFFF5F5F5),
    appBarTheme: const AppBarTheme(
      elevation: 0,
      color: Colors.blueGrey,
      toolbarTextStyle: TextStyle(
          color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal),
      titleTextStyle: TextStyle(
        color: Colors.white,
        fontSize: 18.0,
        fontWeight: FontWeight.w800,
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    ),
    buttonTheme: ButtonThemeData(
        height: 50,
        buttonColor: defaultClr,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        textTheme: ButtonTextTheme.primary),
    iconTheme: const IconThemeData(color: Colors.white),
  );
}

ButtonStyle flatButtonStyle = TextButton.styleFrom(
  backgroundColor: defaultClr,
  padding: const EdgeInsets.all(0),
);
