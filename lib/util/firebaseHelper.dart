import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/util/constant.dart';



class FirebaseHelper{
 FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;
  BuildContext? context;
  static bool isLaunch=false;
  static int nCnt=0;
  Function? onRefreshClick;

  FirebaseHelper(BuildContext context){
    this.context=context;
    onRefreshClick=null;
    flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
  }

  void setRefresh(BuildContext ctx,Function? onrefresh){
    this.context=ctx;
    onRefreshClick=onrefresh;
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  void setPush(bool isSplash){
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      if (message != null) {

      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification!.android;

      if (notification != null){// && android != null && !kIsWeb) {

        flutterLocalNotificationsPlugin!.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              Constant.appName, Constant.appName,channelDescription:Constant.appName,
              icon: 'launch_background',
            ),
          ),
        );
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');

    });
  }

  void updateFCMToken() async {
    FirebaseMessaging.instance.getToken().then((token) async{
      String deviceid = '';
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        deviceid = androidInfo.id;
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        deviceid = iosInfo.identifierForVendor!;
      }
      await updateFCM(deviceid, Platform.operatingSystem, token!);
    });

  }

}