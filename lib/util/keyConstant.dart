import 'package:shared_preferences/shared_preferences.dart';


class KeyConstant {
  static String key_user = "user";
  static String key_theme = "appTheme";
  static bool key_themeLight = false;
  static bool key_themeDark = true;

  static String role_gatekeeper="security_guard";
  static String role_technician="technician";
  static String role_show_checker="impression_attendee";
  static String role_canteen_admin="canteen_admin";

  static Future saveString(String key, String val) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(key, val);
  }

  static Future<String> retriveString(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.containsKey(key)) {
        return preferences.getString(key)!;
    } else
      return '';
  }

  static Future<bool> retriveBool(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.containsKey(key)) {
      return preferences.getBool(key)!;
    }
    else
      return false;
  }
}
