
import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/dialog/imgDialog.dart';
import 'package:sugengatekeeper/dialog/memberDialog.dart';
import 'package:sugengatekeeper/dialog/outTimeDialog.dart';
import 'package:sugengatekeeper/dialog/updateVisitorDialog.dart';
import 'package:sugengatekeeper/model/appointmentPodo.dart';
import 'package:flutter/material.dart';
import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'dart:ui' as ui;

class VistiorDetailScreen extends StatefulWidget {
  String appointmentId;
  VistiorDetailScreen({super.key, required this.appointmentId});

  @override
  _VistiorDetailScreenState createState() => _VistiorDetailScreenState();
}

class _VistiorDetailScreenState extends State<VistiorDetailScreen> {
  Map? res;
  AppointmentPojo? appointmentPojo;
  double fontSize = 16;

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  Future getData() async {
    res = await appointmentDetail(widget.appointmentId);
  //  log(json.encode(res));
    if (res != null) {
      if (res!.containsKey('appointment_data')) {
        appointmentPojo =
            AppointmentPojo.fromJson(res!['appointment_data'] as dynamic);
      } else if (res!.containsKey('message')) {
        showToast(res!['message']);
      }
    }
    if (mounted) setState(() {});
  }

  void openImg(String url) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return ImageDialog(url: url);
        });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail'),
      ),
      body: (res == null)
          ? CustomUI.CustProgress(context)
          : (res!.containsKey('success') &&
                  res!['success'] == Constant.api_no_net)
              ? NoInternetDialog(
                  retryClick: () {
                    res = null;
                    if (mounted) setState(() {});
                  },
                )
              : (res!.containsKey('success') &&
                      res!['success'] == Constant.api_catch_error)
                  ? const Center(
                      child: const Text('Something wrong!!!Try Later'),
                    )
                  : (appointmentPojo != null)
                      ? Column(
                          children: [
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    if(appointmentPojo!.custData!=null && appointmentPojo!.custData!.name!=null)
                                      Container(
                                        margin: const EdgeInsets.all(10),
                                        child: Text(
                                          'Invite By: '+appointmentPojo!.custData!.name!,
                                          style: TextStyle(fontSize: fontSize),
                                        ),
                                      ),
                                    headerRow('Visitor Detail'),
                                    contentRow('Name',
                                        appointmentPojo!.visitorData!.name!),
                                    contentRow('Phone',
                                        appointmentPojo!.visitorData!.phone!),
                                    if (appointmentPojo!.visitorData!.email !=
                                            null &&
                                        appointmentPojo!.visitorData!.email
                                            .toString()
                                            .isNotEmpty)
                                      contentRow('Email',
                                          appointmentPojo!.visitorData!.email!),
                                    contentRow('Gender',
                                        appointmentPojo!.visitorData!.gender!),
                                    if (appointmentPojo!.visitorData!.city !=
                                            null &&
                                        appointmentPojo!.visitorData!.city
                                            .toString()
                                            .isNotEmpty)
                                      contentRow('City',
                                          appointmentPojo!.visitorData!.city!),
                                    if (appointmentPojo!
                                                .visitorData!.companyName !=
                                            null &&
                                        appointmentPojo!
                                            .visitorData!.companyName
                                            .toString()
                                            .isNotEmpty)
                                      contentRow(
                                          'Company',
                                          appointmentPojo!
                                              .visitorData!.companyName!),
                                    if (appointmentPojo!.appointmentDetails !=
                                            null &&
                                        appointmentPojo!.appointmentDetails![0]
                                                .vehicleNo !=
                                            null &&
                                        appointmentPojo!
                                            .appointmentDetails![0].vehicleNo
                                            .toString()
                                            .isNotEmpty)
                                      contentRow(
                                          'Vehicle No',
                                          appointmentPojo!
                                              .appointmentDetails![0]
                                              .vehicleNo!),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: contentRow(
                                              'Category',
                                              appointmentPojo!
                                                  .visitorCategoryName!),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: contentRow('Type',
                                              appointmentPojo!.visitTypeName!),
                                          flex: 1,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: contentRow(
                                              (appointmentPojo!
                                                          .appointmentDetails![
                                                              0]
                                                          .startTime
                                                          .toString() !=
                                                      appointmentPojo!
                                                          .appointmentDetails![
                                                              0]
                                                          .endTime
                                                          .toString())
                                                  ? 'Start DateTime'
                                                  : 'DateTime',
                                              appointmentPojo!
                                                  .appointmentDetails![0]
                                                  .startTime!),
                                          flex: 1,
                                        ),
                                        if (appointmentPojo!
                                                    .appointmentDetails![0]
                                                    .startTime
                                                    .toString() !=
                                                appointmentPojo!
                                                    .appointmentDetails![0]
                                                    .endTime
                                                    .toString() ||
                                            appointmentPojo!.visitTypeName
                                                    .toString()
                                                    .toLowerCase() ==
                                                'date range' || appointmentPojo!.visitTypeName
                                            .toString()
                                            .toLowerCase() ==
                                            'long stay')
                                          Expanded(
                                            child: contentRow(
                                                'End DateTime',
                                                appointmentPojo!
                                                    .appointmentDetails![0]
                                                    .endTime!),
                                            flex: 1,
                                          ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        if (appointmentPojo!.visitorOnboardTime !=
                                            null)
                                          Expanded(
                                            flex: 1,
                                            child: contentRow('In DateTime',
                                                appointmentPojo!.visitorOnboardTime!),
                                          ),
                                        if (appointmentPojo!.visitorOutTime !=
                                            null)
                                          Expanded(
                                            flex: 1,
                                            child: contentRow('Out DateTime',
                                                appointmentPojo!.visitorOutTime!),
                                          ),
                                      ],
                                    ),
                                    contentRow(
                                      'Status',
                                      appointmentPojo!.status
                                          .toString()
                                          .toUpperCase(),
                                    ),
                                    Row(
                                      children: [
                                        if (appointmentPojo!
                                                    .visitorData!.photo !=
                                                null &&
                                            appointmentPojo!
                                                .visitorData!.photo!.isNotEmpty)
                                          Expanded(
                                            child: Column(
                                              children: [
                                                Text('Photo',
                                                    style: TextStyle(
                                                        fontSize: fontSize)),
                                                InkWell(
                                                  onTap: () => openImg(
                                                      appointmentPojo!
                                                          .visitorData!.photo!),
                                                  child: Image.network(
                                                    appointmentPojo!
                                                        .visitorData!.photo!,
                                                    height: 70,
                                                    width: 70,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            flex: 1,
                                          ),
                                        if (appointmentPojo!
                                                    .visitorData!.idProof !=
                                                null &&
                                            appointmentPojo!.visitorData!
                                                .idProof!.isNotEmpty)
                                          Expanded(
                                            child: Column(
                                              children: [
                                                Text('Id Proof',
                                                    style: TextStyle(
                                                        fontSize: fontSize)),
                                                InkWell(
                                                  onTap: () => openImg(
                                                      appointmentPojo!
                                                          .visitorData!
                                                          .idProof!),
                                                  child: Image.network(
                                                    appointmentPojo!
                                                        .visitorData!.idProof!,
                                                    height: 70,
                                                    width: 70,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            flex: 1,
                                          ),
                                      ],
                                    ),
                                    if (appointmentPojo!.visitPurpose != null &&
                                        appointmentPojo!
                                            .visitPurpose!.isNotEmpty)
                                      contentRow(
                                        'Purpose',
                                        appointmentPojo!.visitPurpose
                                            .toString(),
                                      ),
                                    if (appointmentPojo!.gatekeeperRemark !=
                                            null &&
                                        appointmentPojo!
                                            .gatekeeperRemark!.isNotEmpty)
                                      contentRow(
                                        'Remark',
                                        appointmentPojo!.gatekeeperRemark
                                            .toString(),
                                      ),
                                    if (appointmentPojo!.appointmentDetails !=
                                            null &&
                                        appointmentPojo!
                                                .appointmentDetails!.length >
                                            1)
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          headerRow('Member List'),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              physics:
                                                  const NeverScrollableScrollPhysics(),
                                              itemCount: appointmentPojo!
                                                  .appointmentDetails!.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return memberRow(appointmentPojo!
                                                        .appointmentDetails![
                                                    index]);
                                              }),
                                        ],
                                      ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    if (appointmentPojo!.status == 'pending')
                                      Container(
                                        margin: const EdgeInsets.only(
                                            left: 10, right: 10),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: SwiftoBtn(
                                                'Accept',
                                                () async {
                                                  enterRemark('accept');
                                                  //updateStatus('Accept');
                                                },
                                                bgClr: Colors.blueAccent,
                                              ),
                                              flex: 1,
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: SwiftoBtn(
                                                'Reject',
                                                () {
                                                  enterRemark('reject');
                                                  //updateStatus('Reject');
                                                },
                                                bgClr: Colors.redAccent,
                                              ),
                                              flex: 1,
                                            ),
                                          ],
                                        ),
                                      ),
                                    if (appointmentPojo!.status == 'onboard')
                                      Container(
                                        margin: const EdgeInsets.only(
                                            left: 10, right: 10),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: SwiftoBtn('Update Visitor',
                                                  () async {
                                                dynamic r = await showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return UpdateVisitorDialog(
                                                          appointmentPojo:
                                                              appointmentPojo!);
                                                    });
                                                if (r != null &&
                                                    r is bool &&
                                                    r) {
                                                  if (mounted) {
                                                    setState(() {
                                                      res=null;
                                                      getData();
                                                    });
                                                  }
                                                }
                                              }),
                                              flex: 1,
                                            ),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: SwiftoBtn('Complete',
                                                  () async {
                                                dynamic res = await showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return OutTimeDialog(
                                                          appointmentId: widget
                                                              .appointmentId);
                                                    });
                                                if (mounted) {
                                                  if (res != null) {
                                                    if (res is bool && res) {
                                                      setState(() {
                                                        res = null;
                                                        getData();
                                                      });
                                                    } else if (res is String) {
                                                      showToast(res);
                                                    }
                                                  }
                                                }
                                              }),
                                            ),
                                          ],
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      : const Center(
                          child: Text('Data not found'),
                        ),
    );
  }

  Widget headerRow(String title) {
    return Container(
      color: Theme.of(context).cardColor,
      padding: const EdgeInsets.all(10),
      width: MediaQuery.of(context).size.width,
      child: Text(title,
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
    );
  }

  Widget contentRow(String txt, String val) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      child: TextFormField(
        initialValue: val,
        decoration: InputDecoration(hintText: txt, labelText: txt),
        readOnly: true,
        textInputAction: TextInputAction.newline,
        keyboardType: TextInputType.multiline,
      ),
    );
  }

  Widget memberRow(AppointmentDetails data, {bool isHeader= false}) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      child: InkWell(
        onTap: () async {
          if (appointmentPojo!.status == 'onboard') {
            dynamic r = await showDialog(
                context: context,
                builder: (BuildContext context) {
                  return MemberDialog(
                      detail: data,
                      appointmentId: appointmentPojo!.id.toString());
                });
            if (r != null && r is String && r == 'updated') {
              if (mounted) {
                setState(() {
                  res = null;
                  getData();
                });
              }
            }
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    data.name!,
                    style: TextStyle(
                        fontWeight:
                            (isHeader) ? FontWeight.bold : FontWeight.normal,
                        fontSize: fontSize),
                  ),
                  flex: 1,
                ),
                Expanded(
                  child: Text(
                    data.phone!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight:
                            (isHeader) ? FontWeight.bold : FontWeight.normal,
                        fontSize: fontSize),
                  ),
                  flex: 1,
                ),
                Expanded(
                  child: Text(
                    data.gender!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight:
                            (isHeader) ? FontWeight.bold : FontWeight.normal,
                        fontSize: fontSize),
                  ),
                  flex: 1,
                ),
              ],
            ),
            if (data.idProof != null)
              InkWell(
                onTap: () => openImg(data.idProof!),
                child: const Text(
                  'View Id Proof',
                  style: TextStyle(color: Colors.blue),
                  textAlign: TextAlign.left,
                ),
              ),
            if (data.vehicleNo != null && data.vehicleNo!.isNotEmpty)
              Text('Vehicle No:' + data.vehicleNo!),
            if (data.isPresent != null) Text('Present:' + data.isPresent!),
            Row(
              children: [
                if (data.temp != null)
                  Expanded(
                    child: Text('Temperature:' + data.temp!),
                    flex: 1,
                  ),
                if (data.isVaccinated != null && data.isVaccinated!.isNotEmpty)
                  Expanded(
                    child: Text(
                      'Vaccinated:' + data.isVaccinated!,
                      textAlign: ui.TextAlign.center,
                    ),
                    flex: 1,
                  ),
                if (data.isMask != null && data.isMask!.isNotEmpty)
                  Expanded(
                    child: Text(
                      'Wear Mask:' + data.isMask!,
                      textAlign: TextAlign.end,
                    ),
                    flex: 1,
                  ),
              ],
            ),
            if (appointmentPojo!.visitTypeName.toString().toLowerCase() ==
                'date range' || appointmentPojo!.visitTypeName.toString().toLowerCase() ==
                'long stay')
              Row(
                children: [
                  if (data.startTime != null &&
                      data.startTime!.isNotEmpty &&
                      data.startTime.toString() != '0000-00-00 00:00:00')
                    Expanded(
                      child: Text('Start Time:' + data.startTime!),
                      flex: 1,
                    ),
                  if (data.endTime != null &&
                      data.endTime!.isNotEmpty &&
                      data.endTime.toString() != '0000-00-00 00:00:00' &&
                      data.startTime.toString() != data.endTime.toString())
                    Expanded(flex: 1, child: Text('End Time:' + data.endTime!)),
                ],
              ),
            const Divider(),
          ],
        ),
      ),
    );
  }

  Future enterRemark(String act) async {
    String alertMsg = 'Are you sure you want to ', status = '';
    if (act == 'accept') {
      alertMsg = alertMsg + ' Accept?';
      status = 'Accept';
    } else {
      alertMsg = alertMsg + ' Reject?';
      status = 'Reject';
    }
    TextEditingController _remarkCtrl = TextEditingController();
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            insetPadding: EdgeInsets.zero,
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Text(alertMsg),
                  SwiftoTxtField(
                      _remarkCtrl,
                      'Enter Remark',
                      TextInputAction.newline,
                      TextInputType.multiline,
                      true,
                      null,
                      '')
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    //   _remarkCtrl.dispose();
                    Navigator.of(context).pop();
                  },
                  child: const Text('No')),
              TextButton(
                  onPressed: () async {
                    Map? resSt = await updateAppointment(widget.appointmentId,
                        (act.toLowerCase() == 'accept') ? 'onboard' : 'cancel',
                        remark: _remarkCtrl.text);
                    if (resSt != null) {
                      if (resSt.containsKey('success')) {
                        if (resSt['success'] == 1) {
                          // _remarkCtrl.dispose();
                          Navigator.of(context).pop();
                          if (mounted) {
                            setState(() {
                              res = null;
                              getData();
                            });
                          }
                        } else if (resSt.containsKey('message')) {
                          showToast(resSt['message']);
                        }
                      }
                    }
                  },
                  child: const Text('Yes')),
            ],
          );
        });
  }
}
