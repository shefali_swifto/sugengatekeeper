import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/model/appointmentPodo.dart';
import 'package:sugengatekeeper/ui/visitorRow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/util/constant.dart';

class VisitorListScreen extends StatefulWidget {
  String filter;
  VisitorListScreen({Key? key,required this.filter}) : super(key: key);

  @override
  _VisitorListScreenState createState() => _VisitorListScreenState();
}

class _VisitorListScreenState extends State<VisitorListScreen> {
  Map? res;
  List appList = [];
  double fontSize = 16;
  DateTime dt = DateTime.now();

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  Future _selectDate() async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: dt,
      firstDate: DateTime(Constant.min_year, 1),
      lastDate: DateTime(
          DateTime.now().year + 5, DateTime.now().month, DateTime.now().day),
    );
    if (picked != null) {
      dt = picked;
      if (mounted) {
        setState(() {
          res = null;
          getData();
        });
      }
    }
  }

  Future getData() async {
    appList.clear();
    if(widget.filter=='visitors') {
      res = await appointmentList(DateFormat("yyyy-MM-dd").format(dt));
    } else {
      res = await getOnboardList();
    }
    if (res != null && res!.containsKey('data')) {
      List list = res!['data'];
      if (list.isNotEmpty) {
        await Future.wait(list.map((e) async {
          appList.add(AppointmentPojo.fromJson(e));
        }));
      }
    }
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 10), () {
      getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        if(widget.filter=='visitors')
        InkWell(
          onTap: () => _selectDate(),
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(10.0),
            margin: EdgeInsets.only(bottom: 10),
            color: Theme.of(context).cardColor,
            alignment: Alignment.center,
            child: Text(
              DateFormat("dd MMM yyyy").format(dt),
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
        Expanded(
          child: (res == null)
              ? CustomUI.CustProgress(context)
              : (res != null && res!['success'] == Constant.api_no_net)
                  ? NoInternetDialog(
                      retryClick: () {
                        if (mounted) {
                          setState(() {
                            res = null;
                            getData();
                          });
                        }
                      },
                    )
                  : (appList.isEmpty)
                      ? const Center(
                          child: Text('No Data'),
                        )
                      : ListView.builder(
                          shrinkWrap: true,
                          itemCount: appList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return VisitorRow(data: appList[index]);
                          }),
          flex: 1,
        )
      ],
    ));
  }
}
