import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:simple_barcode_scanner/simple_barcode_scanner.dart';
import 'package:sugengatekeeper/main.dart';
import 'package:sugengatekeeper/settingScreen.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:sugengatekeeper/screens/visitor/visitorDetailScreen.dart';
import 'package:sugengatekeeper/screens/visitor/visitorListScreen.dart';

class VisitorScreen extends StatefulWidget {
  const VisitorScreen({Key? key}) : super(key: key);

  @override
  State<VisitorScreen> createState() => _VisitorScreenState();
}

class _VisitorScreenState extends State<VisitorScreen> {
  String username='';

  Future getInfo()async{
    String user=await KeyConstant.retriveString(KeyConstant.key_user);
    if(user.isNotEmpty){
      Map userRes=json.decode(user);
      username=userRes['full_name'];
      if(mounted){
        setState(() {

        });
      }
    }
  }


  @override
  void initState() {
    super.initState();
    getInfo();
    Future.delayed(Duration(milliseconds: 10),(){
      MyApp.firebaseHelper!.updateFCMToken();
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: AppBar(
              title: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(Constant.appName),
                  Text(username)
                ],
              ),
              actions: [
                IconButton(onPressed: ()async{
                  String? cameraScanResult = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SimpleBarcodeScannerPage(),
                      ));
                  if(cameraScanResult!=null && cameraScanResult.contains('/')){
                    List sList=cameraScanResult.split('/');
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) {
                          return VistiorDetailScreen(
                            appointmentId: sList.last.toString(),
                          );
                        },
                      ),
                    );
                  }
                },icon: const Icon(Icons.qr_code_scanner),),
                IconButton(onPressed: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) {
                        return const SettingScreen();
                      },
                    ),
                  );
                }, icon: const Icon(Icons.settings))
              ],
              bottom: const TabBar(
                tabs: [
                  Tab(
                    text: 'Visitors',
                  ),
                  Tab(
                    text: 'Onboard',
                  ),
                ],
              ),
            ),
            body: TabBarView(
              children: [VisitorListScreen(filter:'visitors'), VisitorListScreen(filter:'onboard')],
            )));
  }
}
