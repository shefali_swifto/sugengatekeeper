import 'dart:convert';

import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/main.dart';
import 'package:sugengatekeeper/screens/canteen/bookingList.dart';
import 'package:sugengatekeeper/screens/visitor/visitorScreen.dart';
import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'complain/userComplaintScreen.dart';
import 'movietheater/bookingListScreen.dart';

class VerifyCode extends StatefulWidget {
  final bool fromCart;
  final String? phoneNumber;
  final String? verId;
  Map res;

  VerifyCode({this.fromCart = false, this.verId, this.phoneNumber,required this.res});

  @override
  _LoginSMSState createState() => _LoginSMSState();
}

class _LoginSMSState extends State<VerifyCode> with TickerProviderStateMixin,WidgetsBindingObserver {
  TextEditingController _otpController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  showToast(String msg) {
    ScaffoldMessenger.of(context).showSnackBar(CustomUI.showToast(msg, context));
  }

  void handlePush() {
    MyApp.firebaseHelper!.setRefresh(context,null);
  }

  void setLoginData(Map res)async{
    Map user = res['user_data'];
    user['role_text'] = res['role'];
    await KeyConstant.saveString(
        KeyConstant.key_user, json.encode(user));
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (BuildContext context) {
            return (res['role'] == KeyConstant.role_gatekeeper)
                ? const VisitorScreen()
                : (res['role'] == KeyConstant.role_technician)
                ? const UserComplaintScreen()
                : (res['role'] == KeyConstant.role_show_checker)
                ? BookingListScreen()
                :(res['role']==KeyConstant.role_canteen_admin)
                ? const BookingList()
                : Container();
          },
        ), ModalRoute.withName('/'));
  }

  @override
  void initState() {
    super.initState();
    handlePush();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    _otpController.dispose();
    super.dispose();
  }

  _loginSMS(smsCode, context) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: widget.verId!,
        smsCode: smsCode,
      );
      final User user = (await FirebaseAuth.instance.signInWithCredential(credential)).user!;
      if (user != null) {
        Constant.showLog("===user===");
        Constant.showLog(user.phoneNumber);
        setLoginData(widget.res);
      } else {
        Constant.showLog("===fail2===");
        Constant.showLog("invalidSMSCode");
      }
    } catch (e) {
      Constant.showLog("===fail3===");
      Constant.showLog(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Center(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                SizedBox(height: 100),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    'Phone Number Verification',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
                  child: RichText(
                    text: TextSpan(
                        text: 'Enter the code sent to ',
                        children: [
                          TextSpan(
                              text: widget.phoneNumber,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15)),
                        ],
                        style: TextStyle(fontSize: 15)),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SwiftoTxtField(
                    _otpController,
                    'Enter OTP',
                    TextInputAction.done,
                    TextInputType.number,
                    false,
                    6,
                    null),
                Center(
                  child: SwiftoBtn(
                      'Verify', () {
                    if (_otpController.text.length != 6)
                      showToast('Please enter valid OTP');
                    else
                      _loginSMS(_otpController.text, context);
                  }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      handlePush();
    }
  }

}
