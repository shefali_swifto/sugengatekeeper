import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:simple_barcode_scanner/simple_barcode_scanner.dart';
import 'package:sugengatekeeper/apis/showAPI.dart';
import 'package:sugengatekeeper/dialog/alertDialog.dart';
import 'package:sugengatekeeper/model/bookShowPojo.dart';
import 'package:sugengatekeeper/screens/movietheater/bookingDetailScreen.dart';
import 'package:sugengatekeeper/screens/movietheater/seatDialog.dart';
import 'package:sugengatekeeper/settingScreen.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';

class BookingListScreen extends StatefulWidget {
  const BookingListScreen({Key? key}) : super(key: key);

  @override
  State<BookingListScreen> createState() => _BookingListScreenState();
}

class _BookingListScreenState extends State<BookingListScreen> {
  Map? res;
  List<BookShowPojo> appList = [];
  double fontSize = 16;
  String username = '';

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  Future getInfo() async {
    String user = await KeyConstant.retriveString(KeyConstant.key_user);
    if (user.isNotEmpty) {
      Map userRes = json.decode(user);
      username = userRes['full_name'];
      if (mounted) {
        setState(() {});
      }
    }
  }

  Future getData() async {
    appList.clear();
    res = await bookingList();
    if (res != null && res!.containsKey('data')) {
      List list = res!['data'];
      if (list.isNotEmpty) {
        await Future.wait(list.map((e) async {
          appList.add(BookShowPojo.fromJson(e));
        }));
      }
    }
    if (mounted) setState(() {});
  }

  Future editAttend(String? bookingId,String? url)async{
    String bId='';
    if(bookingId!=null) {
      bId=bookingId;
    } else {
      bId=url!.split('/').last;
    }
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return  BookingDetailScreen(bookingId:bId.toString());
        },
      ),
    );
  }

  Future showSeats(BookShowPojo bookShowPojo) async {
   dynamic r= await showDialog(
        context: context,
        builder: (BuildContext context) {
          return ModifySeatDialog(
            bookShowPojo: bookShowPojo
          );
        });
    if(r!=null && r is String && r=='cancel'){
      cancelTicketBooking(bookShowPojo.id.toString());
    }else if(r!=null && r is String && r=='update'){
      if (mounted) {
        res = null;
        getData();
      }
    }
  }

  Future cancelTicketBooking(String bookingId) async {
    await showDialog(
        context: context,
        builder: (BuildContext context1) {
          return CustomAlertDialog(
            title: 'Cancel Alert!!!',
            msg: 'Are you sure you want to cancel?',
            posClick: () async {
              Navigator.of(context1).pop();
              CustomUI.showProgress(context);
              Map? canRes = await cancelBooking(bookingId);
              CustomUI.hideProgress(context);
              if (canRes != null) {
                if (canRes.containsKey('success')) {
                  if (canRes['success'] == 1) {
                    if (mounted) {
                      setState(() {
                        res == null;
                        getData();
                      });
                    }
                  } else if (canRes.containsKey('message')) {
                    showToast(canRes['message']);
                  }
                }
              }
            },
            negClick: () {
              Navigator.of(context1).pop();
            },
          );
        });
  }

  @override
  void initState() {
    super.initState();
    getInfo();
    Future.delayed(new Duration(milliseconds: 10), () {
      getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [Text(Constant.appName), Text(username)],
        ),
        actions: [
          IconButton(onPressed: ()async{
            String? cameraScanResult = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const SimpleBarcodeScannerPage(),
                ));
            if(cameraScanResult!=null && cameraScanResult.isNotEmpty){
              editAttend(null, cameraScanResult);
            }
          },icon: const Icon(Icons.qr_code_scanner),),
          IconButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) {
                      return const SettingScreen();
                    },
                  ),
                );
              },
              icon: const Icon(Icons.settings))
        ],
      ),
      body: (res == null)
          ? CustomUI.CustProgress(context)
          : (res != null && res!['success'] == Constant.api_no_net)
              ? NoInternetDialog(
                  retryClick: () {
                    if (mounted) {
                      setState(() {
                        res = null;
                        getData();
                      });
                    }
                  },
                )
              : (res != null && res!['success'] == Constant.api_no_net)
                  ? NoInternetDialog(
                      retryClick: () {
                        if (mounted) {
                          setState(() {
                            res = null;
                            getData();
                          });
                        }
                      },
                    )
                  : (res != null && res!['success'] == Constant.api_catch_error)
                      ? Center(
                          child: Text(res!['message']),
                        )
                      : (appList.isEmpty)
                          ? Center(
                              child: Text('No Data'),
                            )
                          : ListView.builder(
                              shrinkWrap: true,
                              itemCount: appList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return rowUI(index);
                              }),
    );
  }

  Widget rowUI(int i) {
    String dt = DateFormat('dd MMM yyyy').format(
        DateFormat('yyyy-MM-dd HH:mm:ss').parse(appList[i].bookingDateTime!));
    String house='';
    if(appList[i].houseData!=null && appList[i].houseData!['house_no']!=null) {
      house = 'HouseNo '+appList[i].houseData!['house_no'];
    }
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  appList[i].showName!,
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    Expanded(
                      flex:1,
                      child: Text(
                        'Seats:' + appList[i].seatNo!,
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    if (appList[i].status != 'cancel' && appList[i].status != 'attended')
                      InkWell(
                          onTap: () {
                            // if (!appList[i].seatNo!.contains(',')) {
                            //   cancelTicketBooking(appList[i].id.toString());
                            // } else {
                              showSeats(appList[i]);
                            //}
                          },
                          child: const Text(
                            'Update',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          )),
                  ],
                ),
                const SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    if (appList[i].custType != null &&
                        appList[i].custType!.isNotEmpty)
                      Expanded(
                        child: Text(
                          appList[i].custType!,
                          style: const TextStyle(fontSize: 16),
                        ),
                        flex: 1,
                      ),
                    Text(
                      appList[i].status!.toUpperCase(),
                      style: const TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                if(appList[i].noOfGuest!=null && appList[i].noOfGuest!='0')
                  Container(
                    margin: EdgeInsets.only(top:3),
                    child: Text(
                      'No of Guests:'+appList[i].noOfGuest!,
                      style: const TextStyle(fontSize: 16),
                    ),
                  ),
                const SizedBox(
                  height: 3,
                ),
                if(house.isNotEmpty)
                  Container(
                    margin: EdgeInsets.only(bottom: 3),
                    child: Text('House:'+house),
                  ),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        dt,
                        style: const TextStyle(fontSize: 16),
                      ),
                      flex: 1,
                    ),
                    if(appList[i].status=='pending' || appList[i].status=='booked')
                      InkWell(
                          onTap: () async {
                            String? cameraScanResult = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const SimpleBarcodeScannerPage(),
                                ));
                            if (cameraScanResult != null &&
                                cameraScanResult.isNotEmpty) {
                              if (cameraScanResult == appList[i].url) {
                                editAttend(appList[i].id.toString(), null);
                              }
                            }
                          },
                          child: const Text(
                            'Scan Ticket',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          )),
                  ],
                ),
              ],
            ),
          ),
          const Divider(
            height: 1,
          ),
        ],
      ),
    );
  }
}
