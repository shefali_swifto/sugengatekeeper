import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/apis/showAPI.dart';
import 'package:sugengatekeeper/dialog/alertDialog.dart';
import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/util/constant.dart';

class BookingDetailScreen extends StatefulWidget {
  String bookingId;
  BookingDetailScreen({Key? key, required this.bookingId}) : super(key: key);

  @override
  State<BookingDetailScreen> createState() => _BookingDetailScreenState();
}

class _BookingDetailScreenState extends State<BookingDetailScreen> {
  Map? res;
  List<String> ss = [], sel = [], red = [];

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  Future getData() async {
    res = await getShowDetail(widget.bookingId.toString());
    if(res!.containsKey('success') && res!['success']==1) {
      dynamic data = res!['booking_data'];
      ss = data['seat_no'].split(',');
      sel= data['seat_no'].split(',');
      red=[];
    }
    if (mounted) {
      setState(() {});
    }
  }

  Future updateSeat()async{
    CustomUI.showProgress(context);
    Map? res = await cancelSeat(
        widget.bookingId.toString(),
        sel.join(','),
        red.join(','));
    CustomUI.hideProgress(context);
    if (res != null && res.containsKey('success')) {
      if (res['success'] == 1) {
        if(mounted) {
          setState((){
            res = null;
            getData();
          });
        }
      }
    }
  }

  Future cancelTicketBooking(String bookingId) async {
    await showDialog(
        context: context,
        builder: (BuildContext context1) {
          return CustomAlertDialog(
            title: 'Cancel Alert!!!',
            msg: 'Are you sure you want to cancel?',
            posClick: () async {
              Navigator.of(context1).pop();
              CustomUI.showProgress(context);
              Map? canRes = await cancelBooking(bookingId);
              CustomUI.hideProgress(context);
              if (canRes != null) {
                if (canRes.containsKey('success')) {
                  if (canRes['success'] == 1) {
                    if (mounted) {
                      setState(() {
                        res == null;
                        getData();
                      });
                    }
                  } else if (canRes.containsKey('message')) {
                    showToast(canRes['message']);
                  }
                }
              }
            },
            negClick: () {
              Navigator.of(context1).pop();
            },
          );
        });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail'),
      ),
      body: (res == null)
          ? CustomUI.CustProgress(context)
          : (res != null && res!['success'] == Constant.api_no_net)
              ? NoInternetDialog(
                  retryClick: () {
                    if (mounted) {
                      setState(() {
                        res = null;
                        getData();
                      });
                    }
                  },
                )
              : (res != null && res!['success'] == Constant.api_catch_error)
                  ? Center(
                      child: Text(res!['message']),
                    )
                  : (res != null && res!.containsKey('booking_data'))
                      ? detailUI()
                      : const SizedBox(),
      bottomSheet: (res != null &&
              res!['success'] == 1 &&
              res!['booking_data']['status'].toString() != 'attended' &&
              res!['booking_data']['status'].toString() != 'cancel')
          ? Container(
              height: 40,
              margin: const EdgeInsets.all(10),
              alignment: Alignment.center,
              child: Row(
                children: [
                  if(red.isNotEmpty)
                    Expanded(child: Container(
                      margin: const EdgeInsets.only(right: 5),
                      child: SwiftoBtn('Update Seat',(){
                        if (red.length == ss.length) {
                          cancelTicketBooking(widget.bookingId);
                        } else {
                          updateSeat();
                        }
                      }),
                    ),flex: 1,),
                  Expanded(
                    flex: 1,
                    child: SwiftoBtn('Attend', () async {
                      CustomUI.showProgress(context);
                      Map result = await updateAttend(widget.bookingId, null);
                      CustomUI.hideProgress(context);
                      if (result != null) {
                        if (result.containsKey('success')) {
                          if (result['success'] == 1) {
                            showToast('Successfully Done');
                            if (mounted) {
                              setState(() {
                                res = null;
                                getData();
                              });
                            }
                          } else if (result.containsKey('message'))
                            showToast(result['message']);
                        }
                      }
                    }),
                  ),
                ],
              ),
            )
          : const SizedBox(),
    );
  }

  Widget detailUI() {
    dynamic data = res!['booking_data'];
    String dt = DateFormat('dd MMM yyyy').format(
        DateFormat('yyyy-MM-dd HH:mm:ss').parse(data['booking_date_time']));
    String sdt = DateFormat('dd MMM yyyy').format(
        DateFormat('yyyy-MM-dd HH:mm:ss').parse(data['show_date_time']));
    String house = '';
    if (data.containsKey('house_data') && data['house_data']['name'] != null) {
      house = 'HouseNo ' + data['house_data']['name'];
    }
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              data['show_name'],
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 3,
            ),
            Text('Show Time:' + sdt),
            const SizedBox(
              height: 3,
            ),
            Text('Booking Time:' + dt),
            const SizedBox(
              height: 3,
            ),
            if (data['status'].toString() == 'attended' ||
                data['status'].toString() == 'cancel')
              Container(
                  margin: const EdgeInsets.only(bottom: 3),
                  child: Text('Seat:' + data['seat_no'])),
            Text('Status:' + data['status'].toString().toUpperCase()),
            const SizedBox(
              height: 3,
            ),
            const SizedBox(
              height: 3,
            ),
            Text(data['customer_name'] + " " + data['customer_phone_no']),
            const SizedBox(
              height: 3,
            ),
            if (house.isNotEmpty)
              Container(
                margin: const EdgeInsets.only(bottom: 3),
                child: Text('House:' + house),
              ),
            if(data['customer_type']!=null && data['customer_type'].toString().isNotEmpty)
              Container(
                margin: EdgeInsets.only(bottom:3),
                child: Text(
                  data['customer_type'].toString(),
                ),
              ),
            if(data['no_of_guests']!=null && data['no_of_guests']!='0')
              Container(
                margin: EdgeInsets.only(bottom:3),
                child: Text(
                  'No of Guests:'+data['no_of_guests'],
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            if (data['status'].toString() != 'attended' &&
                data['status'].toString() != 'cancel')
              ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: ss.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        InkWell(
                          onTap: () {
                            if (sel.contains(ss[index])) {
                              sel.remove(ss[index]);
                              red.add(ss[index]);
                            } else {
                              sel.add(ss[index]);
                              red.remove(ss[index]);
                            }
                            if (mounted) {
                              setState(() {});
                            }
                          },
                          child: Row(
                            children: [
                              Icon((sel.contains(ss[index])
                                  ? Icons.check_box
                                  : Icons.check_box_outline_blank)),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                  flex: 1,
                                  child: Text(
                                    ss[index],
                                    style: const TextStyle(fontSize: 18),
                                  )),
                            ],
                          ),
                        ),
                        const Divider(),
                      ],
                    );
                  }),
          ],
        ),
      ),
    );
  }
}
