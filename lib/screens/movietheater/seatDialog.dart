import 'package:flutter/material.dart';
import 'package:sugengatekeeper/apis/showAPI.dart';
import 'package:sugengatekeeper/model/bookShowPojo.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';

class ModifySeatDialog extends StatefulWidget {
  BookShowPojo bookShowPojo;
  ModifySeatDialog({Key? key,required this.bookShowPojo}) : super(key: key);

  @override
  State<ModifySeatDialog> createState() => _SeatDialogState();
}

class _SeatDialogState extends State<ModifySeatDialog> {
  List<String> ss = [], sel = [], red = [];

  Future updateSeat()async{
    CustomUI.showProgress(context);
    Map? res = await cancelSeat(
        widget.bookShowPojo.id.toString(),
        sel.join(','),
        red.join(','));
    CustomUI.hideProgress(context);
    if (res != null && res.containsKey('success')) {
      if (res['success'] == 1) {
       Navigator.of(context).pop();
      }
    }
  }


  @override
  void initState() {
    super.initState();
    ss=widget.bookShowPojo.seatNo!.split(',');
    sel = widget.bookShowPojo.seatNo!.split(',');
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: SingleChildScrollView(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: rowsUI(),
        )
      ),
      actions: [
        Container(
          alignment: Alignment.centerRight,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (red.isNotEmpty)
                TextButton(
                    onPressed: () async {
                      if (ss.length == red.length) {
                        Navigator.of(context).pop('cancel');
                      } else {
                        CustomUI.showProgress(context);
                        Map? res = await cancelSeat(
                            widget.bookShowPojo.id.toString(),
                            sel.join(','),
                            red.join(','));
                        CustomUI.hideProgress(context);
                        if (res != null && res.containsKey('success')) {
                          if (res['success'] == 1) {
                            Navigator.of(context).pop('update');
                          }
                        }
                      }
                    },
                    child: const Text('Submit')),
              const SizedBox(
                width: 5,
              ),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Close'))
            ],
          ),
        ),
      ],
    );
  }

  List<Widget> rowsUI() {
    List<Widget> rws = [];
    for (int index = 0; index < ss.length; index++) {
      rws.add(Column(
        children: [
          InkWell(
            onTap: () {
              if (sel.contains(ss[index])) {
                sel.remove(ss[index]);
                red.add(ss[index]);
              } else {
                sel.add(ss[index]);
                red.remove(ss[index]);
              }
              if (mounted) {
                setState(() {});
              }
            },
            child: Row(
              children: [
                Icon((sel.contains(ss[index])
                    ? Icons.check_box
                    : Icons.check_box_outline_blank)),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                    flex: 1,
                    child: Text(
                      ss[index],
                      style: const TextStyle(fontSize: 18),
                    )),
              ],
            ),
          ),
          const Divider(),
        ],
      ));
    }
    return rws;
  }
}
