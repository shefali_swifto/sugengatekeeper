import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/apis/canteenAPI.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/util/constant.dart';

class BookingDetail extends StatefulWidget {
  Map book;
  DateTime dt;
   BookingDetail({Key? key,required this.book,required this.dt}) : super(key: key);

  @override
  State<BookingDetail> createState() => _BookingDetailState();
}

class _BookingDetailState extends State<BookingDetail> {
  Map? res;
  List list=[];
  String customerType='',noOfGuest='';

  Future getData() async {
    list.clear();
    res = await getEmpBooking(DateFormat("yyyy-MM-dd").format(widget.dt),widget.book['item_id'].toString());
    if(res!=null && res!.containsKey('data')){
      list=res!['data'];
    }
    if (mounted) {
      setState(() {});
    }
  }


  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(widget.book['dish_name']),
            Text(DateFormat("dd MMM yyyy").format(widget.dt))
          ],
        ),
      ),
      body:(res==null)?CustomUI.CustProgress(context):(res!=null && res!['success']==Constant.api_no_net)?
          NoInternetDialog(retryClick: (){
            res=null;
            if(mounted){
              setState((){});
            }
          },):(res!=null && res!['success']==Constant.api_catch_error)?
          Center(
            child: Text(res!['message']),
          )
          : (list.isNotEmpty)?
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                // if(data['customer_type']!=null && data['customer_type'].toString().isNotEmpty)
                //   Container(
                //     margin: EdgeInsets.only(top:3),
                //     child: Text(
                //       data['customer_type'].toString(),
                //       style: const TextStyle(fontSize: 16),
                //     ),
                //   ),
                // if(data['no_of_guests']!=null && data['no_of_guests']!='0')
                //   Container(
                //     margin: EdgeInsets.only(top:3),
                //     child: Text(
                //       'No of Guests:'+data['no_of_guests'],
                //       style: const TextStyle(fontSize: 16),
                //     ),
                //   ),
                rowUI('Name', 'Total Qty'),
                Expanded(child: ListView.builder(
                  itemCount: list.length,
                    itemBuilder: (BuildContext context,int index){
                  dynamic r= list[index];
                  String emp=r['name'];
                  if(r['emp_code']!=null && r['emp_code'].toString().isNotEmpty) {
                    emp=emp+"(${r['emp_code']})";
                  }
                  return rowUI(emp, r['qty'].toString());
                }))
              ],
            ),
          )
          :SizedBox(),
    );
  }
  
  Widget rowUI(String emp,String qty){
    return Column(
      children: [
        Row(
          children: [
            Expanded(child: Text(emp),flex: 75,),
            Expanded(child:Text(qty,textAlign: TextAlign.center,) ,flex: 25,),
          ],
        ),
        Divider()
      ],
    );
  }
}
