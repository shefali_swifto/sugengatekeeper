import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/apis/canteenAPI.dart';
import 'package:sugengatekeeper/screens/canteen/bookingDetail.dart';
import 'package:sugengatekeeper/settingScreen.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';
import 'package:sugengatekeeper/util/constant.dart';

class BookingList extends StatefulWidget {
  const BookingList({Key? key}) : super(key: key);

  @override
  State<BookingList> createState() => _BookingListState();
}

class _BookingListState extends State<BookingList> {
  List<String> fList = ['Today', 'Tomorrow', 'Custom Date'];
  String filter = '';
  DateTime DT = DateTime.now();
  Map? res;
  List list=[];

  Future getData() async {
    list.clear();
    res = await getBooking(DateFormat("yyyy-MM-dd").format(DT));
    if(res!=null && res!.containsKey('data')){
      list=res!['data'];
    }
    if (mounted) {
      setState(() {});
    }
  }

  Future chooseList() async {
    dynamic r = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.zero,
            contentPadding: EdgeInsets.zero,
            title: Row(
              children: [
                IconButton(onPressed: ()=>Navigator.of(context).pop(), icon: Icon(Icons.close)),
                SizedBox(width: 10,),
                Expanded(flex:1,child: const Text('Select')),
              ],
            ),
            content: ListView.builder(
                shrinkWrap: true,
                itemCount: fList.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).pop(fList[index]);
                    },
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(fList[index]),
                        ),
                        Divider(height: 1,)
                      ],
                    ),
                  );
                }),
          );
        });
    if (r != null && r is String) {
      filter = r;
      if (filter == 'Today') {
        DT = DateTime.now();
      } else if (filter == 'Tomorrow') {
        DT = DateTime(
            DateTime.now().year, DateTime.now().month, DateTime.now().day + 1);
      }
      getData();
    }
  }

  Future _selectDate() async {
    FocusManager.instance.primaryFocus?.unfocus();
    DateTime sel = DateTime.now(), fdt = DateTime.now(), ldt = DateTime.now();
    ldt = DateTime(
        DateTime.now().year, DateTime.now().month + 1, DateTime.now().day);
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: sel,
      firstDate: fdt,
      lastDate: ldt,
    );
    if (picked != null) {
      DT = picked;
      if (mounted) {
        setState(() {});
      }
    }
  }

  @override
  void initState() {
    super.initState();
    filter = fList[0];
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Constant.appName),
        actions: [
          IconButton(onPressed: (){
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) {
                  return const SettingScreen();
                },
              ),
            );
          }, icon: const Icon(Icons.settings))
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: SwiftoValidTxtField(
                    filter,
                    'Filter',
                    TextInputAction.done,
                    TextInputType.text,
                        () {},
                        () {},
                    onClick: () => chooseList(),
                    isReadOnly: true,
                  ),
                ),
                if (filter == fList.last)
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(left: 10),
                      child: SwiftoValidTxtField(
                        DateFormat("dd MMM yyyy").format(DT),
                        'Date',
                        TextInputAction.done,
                        TextInputType.text,
                            () {},
                            () {},
                        onClick: () => _selectDate(),
                        isReadOnly: true,
                      ),
                    ),
                  ),
              ],
            ),
            if (res == null) CustomUI.CustProgress(context),
            if (res != null && res!['success'] == Constant.api_no_net)
              NoInternetDialog(
                retryClick: () {
                  res = null;
                  if (mounted) {
                    setState(() {
                      getData();
                    });
                  }
                },
              ),
            if (res != null && res!['success'] == Constant.api_catch_error)
              Expanded(flex:1,child: Center(child: Text(res!['message']))),
            if (res != null && res!['success'] == 1)
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    const SizedBox(height: 10,),
                    rowUI('Item', 'Total Qty', 'Total Present'),
                    Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                            itemCount: list.length,
                            itemBuilder: (BuildContext context, int index) {
                              dynamic r = list[index];
                              return InkWell(
                                onTap:(){
                                   Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) {
                                        return  BookingDetail(book:r,dt: DT,);
                                      },
                                    ),
                                  );
                                } ,
                                child: rowUI(r['dish_name'], r['total_quantity'].toString(),
                                    r['total_present'].toString()),
                              );
                            }))
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget rowUI(String item, String qty, String status) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Text(item),
                flex: 50,
              ),
              Expanded(
                child: Text(qty,textAlign: TextAlign.center,),
                flex: 25,
              ),
              Expanded(
                child: Text(status,textAlign: TextAlign.center,),
                flex: 25,
              ),
            ],
          ),
          Divider()
        ],
      ),
    );
  }
}
