import 'package:flutter/material.dart';
import 'package:sugengatekeeper/apis/complainAPI.dart';
import 'package:sugengatekeeper/screens/complain/userComplaintDetailScreen.dart';
import 'package:sugengatekeeper/ui/complaintRow.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/util/constant.dart';


class UserComplaintListScreen extends StatefulWidget {
  String type;

  UserComplaintListScreen({Key? key, required this.type}) : super(key: key);

  @override
  _UserComplaintListScreenState createState() => _UserComplaintListScreenState();
}

class _UserComplaintListScreenState extends State<UserComplaintListScreen>
    with WidgetsBindingObserver {
  Map? res;
  List clist = [];
  bool isCall = false;

  void getList() async {
    if (!isCall) {
      isCall = true;
      clist = [];
      res = await getUserComplaintList(widget.type,context);
      if (res != null) {
        if (res!.containsKey('success')) {
          if (res!['success'] == 1 && res!.containsKey('data')) {
            clist = res!['data'];
          }
        }
      }
      isCall = false;
      if (mounted) setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    getList();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: (res == null)
            ? Center(
                child: CustomUI.CustProgress(context),
              )
            : (res!['success'] == Constant.api_no_net)
                ? NoInternetDialog(
                    retryClick: () {
                      if (mounted) {
                        setState(() {
                          res = null;
                          getList();
                        });
                      }
                    },
                  )
                : (res!['success'] == Constant.api_catch_error)
                    ? Center(
                        child: Text(res!['message']),
                      )
                    : (clist != null && clist.isNotEmpty)
                        ? ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: clist.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ComplaintRow(clist[index], () {
                                if (mounted) {
                                  setState(() {
                                    res = null;
                                    getList();
                                  });
                                }
                              },()async{
                                await Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) {
                                    return UserComplaintDetail(cJSON: clist[index]);
                                  },
                                ));
                                if (mounted) {
                                  setState(() {
                                    res = null;
                                    getList();
                                  });
                                }
                              });
                            },
                          )
                        : (clist != null && clist.isEmpty)
                            ? Center(
                                child: Text('No Data'),
                              )
                            : const SizedBox(),
      ),
    );
  }


  @override
  void dispose() {
    super.dispose();
  }
}
