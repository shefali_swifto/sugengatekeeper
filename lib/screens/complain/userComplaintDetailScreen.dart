import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/apis/complainAPI.dart';
import 'package:sugengatekeeper/dialog/imgDialog.dart';
import 'package:sugengatekeeper/dialog/solvedDialog.dart';
import 'package:sugengatekeeper/ui/cmtRow.dart';
import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:sugengatekeeper/util/styles.dart';

class UserComplaintDetail extends StatefulWidget {
  Map? cJSON;

  UserComplaintDetail({super.key, required this.cJSON});

  @override
  _UserComplaintDetailState createState() => _UserComplaintDetailState();
}

class _UserComplaintDetailState extends State<UserComplaintDetail>
    with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  DateFormat defaultfmt = DateFormat("yyyy-MM-dd HH:mm:ss");
  DateFormat dtfmt = DateFormat("dd MMM yy hh:mm aa");
  Color? clr;
  DateTime? dt;
  String otime = '', st = '', adminCmt = '', id = '';
  bool isCall = false;
  TextEditingController _reasonCtrl = TextEditingController();
  List<Widget> cmts = [];
  Map? comp;

  void getCompDetail() async {
    if (!isCall) {
      isCall = true;
      CustomUI.showProgress(context);
      Map res = await getDetail(widget.cJSON!['id'].toString(), context);
      CustomUI.hideProgress(context);
      if (res != null && !res.containsKey('success')) {
        comp = res;
        Constant.showLog(res);
        setData();
      } else if (res.containsKey('message')) {
        showToast(res['message']);
      }
      isCall = false;
      if (mounted) setState(() {});
    }
  }

  void updateStatus(String st, {String? techId,String? cmt}) async {
    CustomUI.showProgress(context);
    Map r = await updateStatusComplaint(widget.cJSON!['id'].toString(), st,null,cmt,context);
    CustomUI.hideProgress(context);
    if (r.containsKey('success')) {
      if (r['success'] == 1) {
        getCompDetail();
      } else if (r.containsKey('message')) {
        showToast(r['message']);
      }
    }
  }

  void setData() async{
    if (cmts == null) {
      cmts = [];
    }
    cmts.clear();
    dt = defaultfmt.parse(comp!['created_at']);
    otime = dtfmt.format(dt!);
    id = widget.cJSON!['id'].toString();
    st = comp!['status'].toString();
    adminCmt = comp!['admin_comment_text'] ?? '';
    String user = await KeyConstant.retriveString(KeyConstant.key_user);
    Map resUser = json.decode(user);
    if (adminCmt != null && adminCmt.isNotEmpty) {
      List ac = json.decode(adminCmt);
      for (Map c1 in ac) {
        cmts.add(CommentRow(c1,resUser['role_text']));
      }
    }
    if (st.toLowerCase() == "acknowledge") {
      clr = acknowledgeClr;
    } else if (st.toLowerCase() == "pending") {
      clr = pendingClr;
    } else if (st.toLowerCase() == "solved") {
      clr = solvedClr;
    }else if (st.toLowerCase() == "complete") {
      clr = completeClr;
    } else if (st.toLowerCase() == "cancel") {
      clr = cancelClr;
    }else if (st.toLowerCase() == "closed") {
      clr = closedClr;
    }
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    super.initState();
    comp = widget.cJSON;
    Future.delayed(const Duration(milliseconds: 10), () {
      getCompDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    String title = comp!['cat_name'], loc = '',techName='';
    if (comp!.containsKey('sub_cat_name')) {
      title = title + ' (' + comp!['sub_cat_name'] + ')';
    }
    if (comp!.containsKey('location')) {
      if (comp!['location'] == 'house') {
        if(comp!['house_data']!=null && comp!['house_data'] is Map && comp!['house_data']['house_no']!=null)
          loc = 'HouseNo '+comp!['house_data']['house_no'];
        else
          loc = 'HouseNo';
      } else {
        loc = 'Generic / Common';
      }
    }
    List imgs = [],solImgs=[];
    if (comp!.containsKey('complaint_images')) {
      if (comp!['complaint_images']!=null && comp!['complaint_images'].toString().isNotEmpty) {
        imgs = json.decode(comp!['complaint_images']);
      }
    }
    if(comp!['technician_data']!=null && comp!['technician_data'] is Map) {
      if(comp!['technician_data']['full_name']!=null)
        techName = comp!['technician_data']['full_name'];
    }
    if (comp!.containsKey('complaint_solution_images')) {
      if (comp!['complaint_solution_images'].toString().isNotEmpty) {
        solImgs = json.decode(comp!['complaint_solution_images']);
      }
    }
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: const Text('Complaint Detail')),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Complaint Number : $id\n' + '$title',
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: Text('Complaint By: '+comp!['customer_name'].toString()+' '+comp!['mobile_no'].toString()),
                            ),
                            if (comp!.containsKey('location'))
                              Container(
                                  margin: EdgeInsets.only(bottom: 5),
                                  child: Text('Location: ' + loc)),
                            Text(comp!['complain_text'],
                                style: const TextStyle(fontSize: 16)),
                            const SizedBox(
                              height: 5,
                            ),
                            if(techName.isNotEmpty)
                              Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Text('Technician: '+techName),
                              ),
                            if(comp!['technician_comment']!=null && comp!['technician_comment'].toString().isNotEmpty)
                              Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Text('Technician Comment: '+comp!['technician_comment'].toString()),
                              ),
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    st.toUpperCase(),
                                    style: TextStyle(color: clr, fontSize: 16),
                                  ),
                                  Text(otime,
                                      style: const TextStyle(fontSize: 16)),
                                ]),
                            const SizedBox(
                              height: 10,
                            ),
                            if (imgs.isNotEmpty)
                              Container(
                                height: 80,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: imgs.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Container(
                                          margin: EdgeInsets.only(right: 7),
                                          child: InkWell(
                                              onTap: () {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return ImageDialog(
                                                          url: imgs[index]);
                                                    });
                                              },
                                              child: Image.network(
                                                imgs[index],
                                                height: 70,
                                                width: 70,
                                              )));
                                    }),
                              ),
                            if (solImgs.isNotEmpty)
                              Container(
                                margin: EdgeInsets.only(top:5,bottom: 5),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Solution Images'),
                                    SizedBox(height: 3,),
                                    Container(
                                      height: 80,
                                      child: ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: solImgs.length,
                                          itemBuilder:
                                              (BuildContext context, int index) {
                                            return Container(
                                                margin: EdgeInsets.only(right: 7),
                                                child: InkWell(
                                                    onTap: () {
                                                      showDialog(
                                                          context: context,
                                                          builder:
                                                              (BuildContext context) {
                                                            return ImageDialog(
                                                                url: solImgs[index]);
                                                          });
                                                    },
                                                    child: Image.network(
                                                      solImgs[index],
                                                      height: 70,
                                                      width: 70,
                                                    )));
                                          }),
                                    ),
                                  ],
                                ),
                              ),
                            if(comp!['feedback']!=null && comp!['feedback'].toString().isNotEmpty)
                              Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: Text('Feedback: '+comp!['feedback'].toString()),
                              ),
                            if(st.toLowerCase()=='closed' && comp!['rating']!=null && comp!['rating'].toString().isNotEmpty && comp!['rating']!=0)
                              Container(
                                margin: const EdgeInsets.only(bottom: 5),
                                child: RatingBarIndicator(
                                  rating:double.parse(comp!['rating'].toString()),
                                  itemBuilder: (context, index) => const Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  itemCount: 5,
                                  itemSize: 30.0,
                                  //   direction: Axis.vertical,
                                ),
                              ),
                            (cmts != null && cmts.isNotEmpty)
                                ? Column(
                                    children: cmts,
                                  )
                                : const SizedBox()
                          ]),
                    )
                  ],
                ),
              ),
            ),
            if (st.toLowerCase() == "in progress")
              SwiftoBtn('Solved', () {
                confirmDialog();
              }),
            if(st.toLowerCase()=='acknowledge')
              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(right: 5),
                  child: SwiftoBtn('In Progress', () async {
                    confirmAlertDialog('Complaint In Progress','Enter Note','in progress');
                  }),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future confirmDialog() async {
    dynamic r=await showDialog(
      context: context,
      builder: (BuildContext context) {
        return SolvedDialog(compId: widget.cJSON!['id'].toString(),);
      },
    );
    if(r!=null && r is bool && r){
      getCompDetail();
    }
  }

  Future confirmAlertDialog(String title,String txt,String status) async{
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
          contentPadding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
          title: Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: SwiftoTxtField(
              _reasonCtrl,
              txt,
              TextInputAction.newline,
              TextInputType.multiline,
              true,
              null,
              null),
          actions: <Widget>[
            TextButton(
              style: flatButtonStyle,
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('Cancel',style: TextStyle(color: btnTextClr),)),
            TextButton(
              style: flatButtonStyle,
                onPressed: () {
                  if (status=='cancel' && _reasonCtrl.text.length == 0) {
                    CustomUI.customAlert('', 'Cancel reason required', context);
                  } else {
                    updateStatus(status,cmt:_reasonCtrl.text);
                  }
                },
                child: Text('Submit',style: TextStyle(color: btnTextClr),),),
          ],
        );
      },
    );
  }

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
