import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sugengatekeeper/main.dart';
import 'package:sugengatekeeper/screens/complain/userComplaintListScreen.dart';
import 'package:sugengatekeeper/settingScreen.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';

class UserComplaintScreen extends StatefulWidget {
  const UserComplaintScreen({Key? key}) : super(key: key);

  @override
  _UserComplaintScreenState createState() => _UserComplaintScreenState();
}

class _UserComplaintScreenState extends State<UserComplaintScreen> {
  String username='';

  Future getInfo()async{
    String user=await KeyConstant.retriveString(KeyConstant.key_user);
    if(user.isNotEmpty){
      Map userRes=json.decode(user);
      username=userRes['full_name'];
      if(mounted){
        setState(() {

        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(Constant.appName),
              Text(username)
            ],
          ),
          actions: [
            IconButton(onPressed: (){
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return const SettingScreen();
                  },
                ),
              );
            }, icon: const Icon(Icons.settings))
          ],
          bottom: const TabBar(
            tabs: [
              Tab(
                text: 'On Going',
              ),
              Tab(
                text: 'History',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            UserComplaintListScreen(type: 'current'),
            UserComplaintListScreen(type: 'history')
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    getInfo();
    Future.delayed(Duration(milliseconds: 10),(){
      MyApp.firebaseHelper!.updateFCMToken();
    });
  }
}
