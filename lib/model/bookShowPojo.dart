class BookShowPojo {
  int? id;
  int? showId;
  int? empId;
  String? seatNo;
  String? bookingDateTime;
  String? status;
  String? showName;
  String? url,custType,noOfGuest;
  Map? houseData;

  BookShowPojo(
      {this.id,
        this.showId,
        this.empId,
        this.seatNo,
        this.bookingDateTime,
        this.status,
        this.showName,this.houseData,this.custType,
        this.url,this.noOfGuest});

  BookShowPojo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    showId = json['show_id'];
    empId = json['emp_id'];
    seatNo = json['seat_no'];
    bookingDateTime = json['booking_date_time'];
    status = json['status'];
    showName = json['show_name'];
    url = json['url'];
    houseData=json['house_data'];
    if(json.containsKey('customer_type'))
      custType=json['customer_type'];
    if(json.containsKey('no_of_guests'))
      noOfGuest=json['no_of_guests'];
  }

}
