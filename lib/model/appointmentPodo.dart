class AppointmentPojo {
  int? id;
  int? visitorId;
  int? custId,totalVisitor;
  String? appointmentDateTime,gatekeeperRemark,visitorOutTime,visitorOnboardTime;
  String? status,url;
  int? visitorTypeId;
  int? visitorCategoryId;
  String? otp;
  String? visitTypeName;
  String? visitorCategoryName,visitPurpose;
  VisitorData? visitorData;
  CustData? custData;
  List<AppointmentDetails>? appointmentDetails;

  AppointmentPojo(
      {this.id,
        this.visitorId,
        this.custId,
        this.appointmentDateTime,
        this.status,this.url,
        this.visitorTypeId,
        this.visitorCategoryId,
        this.otp,
        this.visitTypeName,
        this.visitorCategoryName,
        this.visitorData,this.gatekeeperRemark,this.visitorOutTime,this.visitorOnboardTime,
        this.custData,this.totalVisitor,this.visitPurpose,
        this.appointmentDetails});

  AppointmentPojo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorId = json['visitor_id'];
    custId = json['cust_id'];
    appointmentDateTime = json['appointment_date_time'];
    status = json['status'];
    url=json['url'];
    visitorTypeId = json['visitor_type_id'];
    if(json.containsKey('visit_type_id'))
      visitorTypeId=json['visit_type_id'];
    visitorCategoryId = json['visitor_category_id'];
    otp = json['otp'];
    visitTypeName = json['visit_type_name'];
    visitorCategoryName = json['visitor_category_name'];
    visitorData = json['visitor_data'] != null
        ? new VisitorData.fromJson(json['visitor_data'])
        : null;
    custData = json['cust_data'] != null
        ? new CustData.fromJson(json['cust_data'])
        : null;
    if (json['appointment_details'] != null) {
      appointmentDetails = <AppointmentDetails>[];
      json['appointment_details'].forEach((v) {
        appointmentDetails!.add(new AppointmentDetails.fromJson(v));
      });
      totalVisitor=appointmentDetails!.length;
    }
    if(json['total_visitor']!=null)
      totalVisitor=json['total_visitor'];
    if(json['visit_purpose']!=null)
      visitPurpose=json['visit_purpose'];
    if(json['gatekeeper_remark']!=null)
      gatekeeperRemark=json['gatekeeper_remark'];
    if(json['visitor_out_time']!=null)
      visitorOutTime=json['visitor_out_time'];
    if(custData==null)
      custData=CustData();
    if(json['customer_name']!=null)
      custData!.name=json['customer_name'];
    if(json['customer_phone_no']!=null)
      custData!.phoneNo=json['customer_phone_no'];
    if(json['visitor_onboard_time']!=null) {
      visitorOnboardTime=json['visitor_onboard_time'];
    }
  }

// Map<String, dynamic> toJson() {
//   final Map<String, dynamic> data = new Map<String, dynamic>();
//   data['id'] = this.id;
//   data['visitor_id'] = this.visitorId;
//   data['cust_id'] = this.custId;
//   data['appointment_date_time'] = this.appointmentDateTime;
//   data['status'] = this.status;
//   data['url']=this.url;
//   data['visitor_type_id'] = this.visitorTypeId;
//   data['visitor_category_id'] = this.visitorCategoryId;
//   data['otp'] = this.otp;
//   data['visit_type_name'] = this.visitTypeName;
//   data['visitor_category_name'] = this.visitorCategoryName;
//   if (this.visitorData != null) {
//     data['visitor_data'] = this.visitorData!.toJson();
//   }
//   if (this.custData != null) {
//     data['cust_data'] = this.custData!.toJson();
//   }
//   if (this.appointmentDetails != null) {
//     data['appointment_details'] =
//         this.appointmentDetails!.map((v) => v.toJson()).toList();
//   }
//   if(this.totalVisitor!=null)
//     data['total_visitor']=this.totalVisitor;
//   return data;
// }
}

class VisitorData {
  String? name;
  String? phone;
  String? altPhone;
  String? photo;
  String? idProof;
  String? email;
  String? gender;
  String? city;
  String? companyName;
  VisitorData({this.name,
    this.phone,
    this.altPhone,
    this.photo,
    this.idProof,
    this.email,
    this.gender,
    this.city,
    this.companyName  });

  VisitorData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phone = json['phone'];
    altPhone = json['alt_phone'];
    photo = json['photo'];
    idProof = json['id_proof'];
    email = json['email'];
    gender = json['gender'];
    city = json['city'];
    companyName = json['company_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['alt_phone'] = this.altPhone;
    data['photo'] = this.photo;
    data['id_proof'] = this.idProof;
    data['email'] = this.email;
    data['gender'] = this.gender;
    data['city'] = this.city;
    data['company_name'] = this.companyName;
    return data;
  }

}

class CustData {
  String? name;
  String? phoneNo;
  String? email,startTime,endTime;

  CustData({this.name, this.phoneNo, this.email,this.startTime,this.endTime});

  CustData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phoneNo = json['phone_no'];
    email = json['email'];
    if(json.containsKey('start_time'))
      startTime=json['start_time'];
    if(json.containsKey('end_time'))
      endTime=json['end_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone_no'] = this.phoneNo;
    data['email'] = this.email;
    data['start_time']=this.startTime;
    data['end_time']=this.endTime;
    return data;
  }
}

class AppointmentDetails {
  int? id;
  int? visitorId;
  int? custId;
  String? vehicleNo;
  String? temp,name,phone,gender,idProof;
  int? mainVisitorId;
  int? appointmentId;
  String? isPresent;
  String? startTime;
  String? endTime;
  String? isVaccinated;
  String? isMask;
  String? createdAt;
  String? updatedAt;

  AppointmentDetails(
      {this.id,
        this.visitorId,
        this.custId,this.name,this.gender,this.phone,this.idProof,
        this.vehicleNo,
        this.temp,
        this.mainVisitorId,
        this.appointmentId,
        this.isPresent,
        this.startTime,
        this.endTime,
        this.isVaccinated,
        this.isMask,
        this.createdAt,
        this.updatedAt});

  AppointmentDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorId = json['visitor_id'];
    custId = json['cust_id'];
    vehicleNo = json['vehicle_no'];
    temp = json['temp'];
    mainVisitorId = json['main_visitor_id'];
    appointmentId = json['appointment_id'];
    isPresent = json['is_present'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    isVaccinated = json['is_vaccinated'];
    isMask = json['is_mask'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    name=json['name'];
    phone=json['phone'];
    gender=json['gender'];
    if(json.containsKey('id_proof'))
      idProof=json['id_proof'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['visitor_id'] = this.visitorId;
    data['cust_id'] = this.custId;
    data['vehicle_no'] = this.vehicleNo;
    data['temp'] = this.temp;
    data['main_visitor_id'] = this.mainVisitorId;
    data['appointment_id'] = this.appointmentId;
    data['is_present'] = this.isPresent;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['is_vaccinated'] = this.isVaccinated;
    data['is_mask'] = this.isMask;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['name']=this.name;
    data['phone']=this.phone;
    data['gender']=this.gender;
    data['id_proof']=this.idProof;
    return data;
  }

  Map<String, dynamic> toJson1() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    if(this.idProof!=null)
      data['id_proof']=this.idProof;
    return data;
  }
}