
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppModel with ChangeNotifier {
  Map<String, dynamic>? appConfig;
  bool darkTheme = false, isLoading = false;

  AppModel() {
    getConfig();
  }

  Future<bool> getConfig() async {
    isLoading = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(KeyConstant.key_theme)) {
      darkTheme = prefs.getBool(KeyConstant.key_theme)!;
    }
    isLoading = false;
    notifyListeners();
    return true;
  }

  void updateTheme(bool theme) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      darkTheme = theme;
      await prefs.setBool(KeyConstant.key_theme, theme);
      notifyListeners();
    } catch (e) {}
  }
}

class App {
  Map<String, dynamic> appConfig;

  App(this.appConfig);
}
