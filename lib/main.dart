import 'package:firebase_core/firebase_core.dart';
import 'package:sugengatekeeper/model/app.dart';
import 'package:sugengatekeeper/splashScreen.dart';
import 'package:sugengatekeeper/util/firebaseHelper.dart';
import 'package:sugengatekeeper/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp();
  runApp(ChangeNotifierProvider.value(
    value: AppModel(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  static final navigatorKey=new GlobalKey<NavigatorState>();
  static FirebaseHelper? firebaseHelper;

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyApp.firebaseHelper = new FirebaseHelper(context);
    if(Provider.of<AppModel>(context).isLoading) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(body: Container()),
      );
    }
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: Provider.of<AppModel>(context).darkTheme
          ? buildDarkTheme().copyWith()
          : buildLightTheme().copyWith(),

    );
  }
}
