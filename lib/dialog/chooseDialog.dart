import 'package:flutter/material.dart';

class ChooseDialog extends StatelessWidget {
  const ChooseDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: SingleChildScrollView(
        child: Column(
          children: [
            InkWell(
              onTap: (){
                Navigator.of(context).pop('camera');
              },
              child: rowUI(Icons.camera_alt, 'Camera'),
            ),
            Divider(),
            InkWell(
              onTap: (){
                Navigator.of(context).pop('gallery');
              },
              child: rowUI(Icons.photo_library, 'Gallery'),
            )
          ],
        ),
      ),
    );
  }

  Widget rowUI(IconData icon,String txt){
    return Row(
      children: [
        Icon(icon),
        SizedBox(width: 10,),
        Text(txt,style: TextStyle(fontSize: 18,),)
      ],
    );
  }
}
