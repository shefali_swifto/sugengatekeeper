import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';

class OutTimeDialog extends StatefulWidget {
  String appointmentId;
  OutTimeDialog({Key? key, required this.appointmentId}) : super(key: key);

  @override
  State<OutTimeDialog> createState() => _OutTimeDialogState();
}

class _OutTimeDialogState extends State<OutTimeDialog> {
  TextEditingController _dtCtrl = new TextEditingController();
  DateTime dt = DateTime.now();

  Future<DateTime?> _selectDate(DateTime dtm) async {
    DateTime sel = dtm, fdt = DateTime.now(), ldt = DateTime.now();
    fdt =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    ldt = DateTime(
        DateTime.now().year, DateTime.now().month + 1, DateTime.now().day);

    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: sel,
      firstDate: fdt,
      lastDate: ldt,
    );
    if (picked != null) {
      TimeOfDay? tpicked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay(hour: picked.hour, minute: picked.minute),
      );
      if (tpicked != null) {
        int _hour = tpicked.hour;
        int _minute = tpicked.minute;
        DateTime fdt1 =
            DateTime(picked.year, picked.month, picked.day, _hour, _minute);
        return fdt1;
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    _dtCtrl.text = DateFormat("dd MMM yyyy HH:mm:ss").format(dt);
    return AlertDialog(
      title: const Text('Alert!!!'),
      content: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('You want to Complate?'),
            SizedBox(
              height: 20,
            ),
            SwiftoValidTxtField(
              _dtCtrl.text,
              'OutTime',
              TextInputAction.done,
              TextInputType.text,
              () {},
              () {},
              onClick: () async {
                DateTime? seldt = await _selectDate(dt);
                _dtCtrl.text = DateFormat("dd MMM yyyy HH:mm:ss").format(dt);
                if (seldt != null) {
                  dt = seldt;
                  if (mounted) setState(() {});
                }
              },
            )
          ],
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: const Text('No')),
        const SizedBox(
          width: 10,
        ),
        TextButton(
            onPressed: () async {
              Map? resSt = await updateAppointment(
                  widget.appointmentId, 'complete',
                  outTime: DateFormat("yyyy-MM-dd HH:mm:ss").format(dt));
              if (resSt != null) {
                if (resSt.containsKey('success')) {
                  if (resSt['success'] == 1) {
                    Navigator.of(context).pop(true);
                  } else if (resSt.containsKey('message')) {
                    Navigator.of(context).pop(resSt['message']);
                  }
                }
              }
            },
            child: const Text('Yes')),
      ],
    );
  }

  @override
  void dispose() {
    _dtCtrl.dispose();
  }
}
