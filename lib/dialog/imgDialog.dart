import 'dart:io';

import 'package:flutter/material.dart';

class ImageDialog extends StatelessWidget {
  File? f;
  String? url;
  ImageDialog({Key? key, this.f, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        insetPadding: EdgeInsets.zero,
        contentPadding: EdgeInsets.zero,
        content: Stack(
          children: [
            (f != null) ? Image.file(f!) : Image.network(url!),
            Positioned(
                right: 0,
                top: 0,
                child:Container(
                    padding: const EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    constraints: const BoxConstraints(
                      minWidth: 30,
                      minHeight: 30,
                    ),
                    child: InkWell(
                      child: const Icon(Icons.close),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    )))
          ],
        ));
  }
}
