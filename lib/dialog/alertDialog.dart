import 'package:flutter/material.dart';

class CustomAlertDialog extends StatefulWidget {
  String title,msg;
  Function? posClick,negClick;
  CustomAlertDialog({Key? key,required this.title,required this.msg,required this.posClick,this.negClick}) : super(key: key);

  @override
  _AlertDialogState createState() => _AlertDialogState();
}

class _AlertDialogState extends State<CustomAlertDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      content: Text(widget.msg),
      actions: [
        if(widget.negClick!=null)
          TextButton(onPressed: ()=>widget.negClick!(), child: Text('No')),
        SizedBox(width: 10,),
        TextButton(onPressed: ()=>widget.posClick!(), child: Text('Yes')),
      ],
    );
  }
}
