import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simple_s3/simple_s3.dart';
import 'package:sugengatekeeper/apis/complainAPI.dart';
import 'package:sugengatekeeper/dialog/chooseDialog.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/styles.dart';

class SolvedDialog extends StatefulWidget {
  String compId;
  SolvedDialog({Key? key, required this.compId}) : super(key: key);

  @override
  State<SolvedDialog> createState() => _SolvedDialogState();
}

class _SolvedDialogState extends State<SolvedDialog> {
  TextEditingController _ctrl = TextEditingController();
  List<XFile>? images;

  void updateStatus(String st, {String? cmt}) async {
    CustomUI.showProgress(context);
    List? imgs;
    if (images != null) {
      imgs = [];
      SimpleS3 _simpleS3 = SimpleS3();
      for (int i = 0; i < images!.length; i++) {
        XFile e = images![i];
        String r = await _simpleS3.uploadFile(
            File(e.path),
            'swiftomatics',
            'ap-south-1:05a45769-ad74-48cf-9f31-cda0391bd6e0',
            AWSRegions.apSouth1);
        imgs.add(r);
      }
    }
    Map r = await updateStatusComplaint(widget.compId, st, imgs, cmt, context);
    CustomUI.hideProgress(context);
    if (r.containsKey('success')) {
      if (r['success'] == 1) {
        Navigator.of(context).pop(true);
      } else if (r.containsKey('message')) {
        showToast(r['message']);
      }
    }
  }

  Future chooseLib() async {
    dynamic r = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return const ChooseDialog();
        });
    if (r != null && r is String) {
      List<XFile>? images1;
      if (r == 'camera') {
        XFile? photo = await ImagePicker().pickImage(
            source: ImageSource.camera, imageQuality: Constant.imgSize);
        images1 = [];
        images1.add(photo!);
      } else if (r == 'gallery') {
        images1 =
            await ImagePicker().pickMultiImage(imageQuality: Constant.imgSize);
      }
      if (images1 != null && images1.isNotEmpty) {
        images ??= [];
        images!.addAll(images1);
        if (images!.length > 5) {
          images = images!.sublist(0, 5);
          showToast("Allow Max 5");
        }
        if (mounted) setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
      contentPadding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      title: const Text(
        'Complaint Alert!!!',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      content: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Are you sure complaint solved?'),
          SwiftoTxtField(_ctrl, 'Comment', TextInputAction.newline,
              TextInputType.multiline, true, null, ''),
          InkWell(
              onTap: () => chooseLib(),
              child: const Padding(
                padding: EdgeInsets.only(top: 8, bottom: 8),
                child: Text(
                  'Attach Photos (Max. 5)',
                  style: TextStyle(color: Colors.blue),
                ),
              )),
          if (images != null)
            Container(
              height: 70,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: images!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin: const EdgeInsets.only(right: 5),
                      child: Stack(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 7, right: 7),
                            child: Image.file(
                              File(images![index].path),
                              height: 70,
                              width: 70,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Positioned(
                              top: 0,
                              right: 0,
                              child: InkWell(
                                onTap: () {
                                  if (mounted) {
                                    images!.removeAt(index);
                                    setState(() {});
                                  }
                                },
                                child: const Icon(
                                  Icons.cancel,
                                  color: Colors.red,
                                ),
                              ))
                        ],
                      ),
                    );
                  }),
            ),
        ],
      )),
      actions: <Widget>[
        TextButton(
            style: flatButtonStyle,
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text(
              'Cancel',
              style: TextStyle(color: btnTextClr),
            )),
        TextButton(
            style: flatButtonStyle,
            onPressed: () {
              //Navigator.pop(context);
              updateStatus('solved', cmt: _ctrl.text);
            },
            child: Text('Submit',style: TextStyle(color: btnTextClr),)),
      ],
    );
  }

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  @override
  void dispose() {
    _ctrl.dispose();
  }
}
