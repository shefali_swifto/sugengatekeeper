import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/model/appointmentPodo.dart';
import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MemberDialog extends StatefulWidget {
  String appointmentId;
  AppointmentDetails detail;
  MemberDialog({Key? key, required this.detail, required this.appointmentId})
      : super(key: key);

  @override
  _MemberDialogState createState() => _MemberDialogState();
}

class _MemberDialogState extends State<MemberDialog> {
  String vehicleNo = '',
      isPresent = '',
      temp = '',
      vacc = '',
      mask = '',
      sdt = '',
      edt = '';
  DateTime? stdt, endt;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Future<Null> _selectDate(BuildContext context, String txt) async {
    DateTime sel = DateTime.now();
    int yr = sel.year + 1;
    if (txt == 'start') {
      if (stdt != null)
        sel =stdt!;
    } else if (endt != null)
      sel = endt!;
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: sel,
      firstDate: DateTime(Constant.min_year, 1),
      lastDate: DateTime(
          DateTime.now().year+1, DateTime.now().month, DateTime.now().day),
    );
    if (picked != null) {
      if (txt == 'start') {
        stdt = picked;
      } else {
        endt =picked;
      }
      if (mounted)
        setState(() {});
    }
  }

  @override
  void initState() {
    if (widget.detail.vehicleNo != null) {
      vehicleNo = widget.detail.vehicleNo!;
    }
    if (widget.detail.isPresent != null) {
      isPresent = widget.detail.isPresent!;
    }
    if (widget.detail.temp != null) {
      temp = widget.detail.temp!;
    }

    if (widget.detail.isVaccinated != null) {
      vacc = widget.detail.isVaccinated!;
    }

    if (widget.detail.isMask != null) {
      mask = widget.detail.isMask!;
    }

    if (widget.detail.startTime != null && widget.detail.startTime.toString()!='0000-00-00 00:00:00') {
      String sttm = widget.detail.startTime!;
      if (sttm.isNotEmpty) {
        stdt = DateFormat("yyyy-MM-dd").parse(sttm);
      }
    }

    if (widget.detail.endTime != null && widget.detail.endTime.toString()!='0000-00-00 00:00:00') {
      String entm = widget.detail.endTime!;
      if (entm.isNotEmpty) {
        endt = DateFormat("yyyy-MM-dd").parse(entm);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      title: Text(widget.detail.name! +
          '\t' +
          widget.detail.phone!,textAlign: TextAlign.center,),
      content: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              SwiftoValidTxtField(vehicleNo, "Vehicle No", TextInputAction.next,
                  TextInputType.text, (input) => vehicleNo = input, (input) {}),
              SwiftoValidTxtField(temp, "Temperature", TextInputAction.next,
                  TextInputType.number, (input) => temp = input, (input) {}),
              rUI('Present', isPresent, (String val) {
                isPresent = val;
                if (mounted) {
                  setState(() {});
                }
              }),
              rUI('Vaccinated', vacc, (String val) {
                vacc = val;
                if (mounted) {
                  setState(() {});
                }
              }),
              rUI('Wear Mask', mask, (String val) {
                mask = val;
                if (mounted) {
                  setState(() {});
                }
              }),
              SwiftoValidTxtField(
                (stdt != null) ? DateFormat("dd MMM yyyy").format(stdt!) : '',
                "Start Time",
                TextInputAction.next,
                TextInputType.text,
                (input) {
                  if(stdt!=null)
                  sdt = DateFormat("yyyy-MM-dd").format(stdt!);
                },
                (input) {},
                isReadOnly: true,
                onClick: ()=>_selectDate(context, 'start'),
              ),
              SwiftoValidTxtField(
                (endt != null) ? DateFormat("dd MMM yyyy").format(endt!) : '',
                "End Time",
                TextInputAction.done,
                TextInputType.text,
                (input) {
                  if(endt!=null)
                  edt = DateFormat("yyyy-MM-dd").format(endt!);
                },
                (input) {},
                isReadOnly: true,
                onClick: ()=>_selectDate(context, 'end')
              ),
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('Cancel')),
        TextButton(
            onPressed: () async {
              formKey.currentState!.save();
              Map? res = await updateAppointmentDetail(
                  widget.appointmentId, widget.detail.id.toString(),
                  vehicleNo: vehicleNo,
                  temp: temp,
                  present: isPresent,
                  sttm: sdt,
                  endtm: edt,
                  isVacc: vacc,
                  isMask: mask);
              if(res!=null && res.containsKey('success')){
                if(res['success']==1) {
                  Navigator.of(context).pop('updated');
                } else if(res.containsKey('message')){
                  ScaffoldMessenger.of(context)
                      .showSnackBar(CustomUI.showToast(res['message'], context));
                }
              }

            },
            child: const Text('Save')),
      ],
    );
  }

  Widget rUI(String txt, String val, Function onClick) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Container(
                width:70,
                  child: Text(txt)),
              const SizedBox(
                width: 10,
              ),
              SwiftoRadio(
                  isSelect: ('yes' == val),
                  txt: 'Yes',
                  onChange: () => onClick('yes')),
              const SizedBox(
                width: 10,
              ),
              SwiftoRadio(
                  isSelect: ('no' == val), txt: 'No', onChange: () => onClick('no')),
            ],
          ),
        ),
        Divider(height: 1,)
      ],
    );
  }
}
