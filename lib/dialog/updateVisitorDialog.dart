import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simple_s3/simple_s3.dart';
import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/dialog/chooseDialog.dart';
import 'package:sugengatekeeper/dialog/imgDialog.dart';
import 'package:sugengatekeeper/model/appointmentPodo.dart';
import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';

import '../util/constant.dart';

class UpdateVisitorDialog extends StatefulWidget {
  AppointmentPojo appointmentPojo;
  UpdateVisitorDialog({Key? key, required this.appointmentPojo})
      : super(key: key);

  @override
  State<UpdateVisitorDialog> createState() => _UpdateVisitorDialogState();
}

class _UpdateVisitorDialogState extends State<UpdateVisitorDialog> {
  String gender = '', email = '', vehicleno = '';
  String? photoUrl, idproofUrl;
  TextEditingController mailCtrl = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  File? pic, idproof;

  Future updateVisitor() async {
    CustomUI.showProgress(context);
    SimpleS3 _simpleS3 = SimpleS3();
    String profileResult = '', idResult = '';
    if (pic != null) {
      profileResult = await _simpleS3.uploadFile(
          pic!,
          'swiftomatics',
          'ap-south-1:05a45769-ad74-48cf-9f31-cda0391bd6e0',
          AWSRegions.apSouth1 //AWSRegions.US_EAST_2//
          );
    }
    if (idproof != null) {
      idResult = await _simpleS3.uploadFile(
          idproof!,
          'swiftomatics',
          'ap-south-1:05a45769-ad74-48cf-9f31-cda0391bd6e0',
          AWSRegions.apSouth1 //AWSRegions.US_EAST_2//
          );
    }
    Map? res = await editVisitor(
        widget.appointmentPojo.id.toString(),
        widget.appointmentPojo.appointmentDetails![0].visitorId.toString(),
        gender,
        email,
        (profileResult.isNotEmpty)
            ? profileResult
            : (photoUrl != null)
                ? photoUrl
                : "",
        (idResult.isNotEmpty)
            ? idResult
            : (idproofUrl != null)
                ? idproofUrl
                : "",
        vehicleno);
    CustomUI.hideProgress(context);
    if(res!=null){
      if(res.containsKey('success') && res['success']==1)
        Navigator.of(context).pop(true);
      else if(res.containsKey('message'))
        showToast(res['message']);
    }
  }

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  void openImg(File? file, String? url) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return ImageDialog(f: file, url: url);
        });
  }

  Future chooseLib(String act) async {
    dynamic r = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return const ChooseDialog();
        });
    if (r != null && r is String) {
      XFile? photo;
      if (r == 'camera') {
        photo = await ImagePicker().pickImage(source: ImageSource.camera, imageQuality: Constant.imgSize);
      } else if (r == 'gallery') {
        photo = await ImagePicker().pickImage(source: ImageSource.gallery, imageQuality: Constant.imgSize);
      }
      if (photo != null) {
        if (act == 'photo') {
          pic = File(photo.path);
        } else {
          idproof = File(photo.path);
        }
        if (mounted) setState(() {});
      }
    }
  }

  @override
  void initState() {
    super.initState();
    gender = widget.appointmentPojo.visitorData!.gender!.toLowerCase();
    photoUrl = widget.appointmentPojo.visitorData!.photo;
    idproofUrl = widget.appointmentPojo.visitorData!.idProof;
    email = widget.appointmentPojo.visitorData!.email!;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      titlePadding: EdgeInsets.zero,
      title: AppBar(
        title: Text('Update Visitor'),
        titleSpacing: 0,
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: Icon(Icons.close),
        ),
      ),
      content: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              SwiftoValidTxtField(
                email,
                'Enter Email',
                TextInputAction.done,
                TextInputType.emailAddress,
                (input) => email = input,
                (input) {
                  if (input!.isNotEmpty &&
                      (!input.contains('.') || !input.contains('@'))) {
                    return 'Invalid Email';
                  } else {
                    return null;
                  }
                },
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 16),
                child: Row(
                  children: [
                    const Text(
                      'Gender',
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 14, letterSpacing: 0.50),
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    SwiftoRadio(
                        isSelect: (gender == 'male'),
                        txt: 'Male',
                        onChange: () {
                          if (gender != 'male') {
                            gender = 'male';
                            if (mounted) setState(() {});
                          }
                        }),
                    const SizedBox(
                      width: 5,
                    ),
                    SwiftoRadio(
                        isSelect: (gender == 'female'),
                        txt: 'Female',
                        onChange: () {
                          if (gender != 'female') {
                            gender = 'female';
                            if (mounted) setState(() {});
                          }
                        }),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text('Photo'),
                          InkWell(
                            onTap: () => chooseLib('photo'),
                            onDoubleTap: () {
                              if (pic != null) {
                                openImg(pic!, null);
                              } else if (photoUrl != null) {
                                openImg(null, photoUrl!);
                              }
                            },
                            child: Container(
                              height: 100,
                              width: 100,
                              child: (pic != null)
                                  ? Image.file(pic!)
                                  : (photoUrl != null && photoUrl!.isNotEmpty)
                                      ? Image.network(photoUrl!)
                                      : const Icon(
                                          Icons.add_photo_alternate,
                                          size: 100,
                                        ),
                            ),
                          ),
                        ],
                      ),
                      flex: 1,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text('ID Proof'),
                          InkWell(
                            onTap: () => chooseLib('idproof'),
                            onDoubleTap: () {
                              if (idproof != null) {
                                openImg(idproof!, null);
                              } else if (idproofUrl != null) {
                                openImg(null, idproofUrl!);
                              }
                            },
                            child: Container(
                              height: 100,
                              width: 100,
                              child: (idproof != null)
                                  ? Image.file(idproof!)
                                  : (idproofUrl != null && idproofUrl!.isNotEmpty)
                                      ? Image.network(idproofUrl!)
                                      : const Icon(
                                          Icons.add_photo_alternate,
                                          size: 100,
                                        ),
                            ),
                          ),
                        ],
                      ),
                      flex: 1,
                    ),
                  ],
                ),
              ),
              SwiftoValidTxtField(
                vehicleno,
                'Enter Vehicle No',
                TextInputAction.done,
                TextInputType.text,
                (input) => vehicleno = input,
                (input) {},
              ),
              Center(
                child: SwiftoBtn('Submit', () {
                  formKey.currentState!.save();
                  updateVisitor();
                }),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    mailCtrl.dispose();
  }
}
