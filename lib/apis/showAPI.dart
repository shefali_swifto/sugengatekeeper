import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sugengatekeeper/util/checkConnection.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';

Future<Map?> bookingList() async {
  try {
    Constant.showLog('List Booking');
    String url = Constant.base_url + "get_today_booking_list";
    Constant.showLog(url);

    bool conn = await isConnection();
    if (conn) {
      final response = await http
          .post(Uri.parse(url), headers: {"brand-id": Constant.brand_id});
      List list = json.decode(response.body);
      Constant.showLog('list booking result---');
      Constant.showLog(list);
      return {'success': 1, 'data': list};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map> updateAttend(String? id, String? scanurl) async {
  try {
    String url = Constant.base_url + "attend_show";
    Constant.showLog('---attend show---');
    Constant.showLog(url);
    Map reqdata;
    if (id != null) {
      reqdata = {'booking_id': id};
    } else {
      reqdata = {'booking_id': scanurl};
    }
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog("attend show");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message':  'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> cancelSeat(String bookId,String seat,String reduceSeat) async {
  try {
    Constant.showLog('Cancel seats');
    String url = Constant.base_url + "update_seats";
    Constant.showLog(url);
    Map req = {'booking_id': bookId,'cancel_seat_no':reduceSeat,'seat_no':seat};
    bool conn = await isConnection();
    if (conn) {
      final response = await http
          .post(Uri.parse(url),
          body: req,
          headers: {"brand-id": Constant.brand_id});
      Constant.showLog('cancel ticket result---');
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> cancelBooking(String bookId) async {
  try {
    Constant.showLog('Cancel Booking');
    String url = Constant.base_url + "cancel_booking";
    Constant.showLog(url);
    Map req = {'booking_id': bookId};
    bool conn = await isConnection();
    if (conn) {
      final response = await http
          .post(Uri.parse(url),
          body: req,
          headers: {"brand-id": Constant.brand_id});
      Constant.showLog('cancel booking result---');
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message':  'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map> getShowDetail(String id) async {
  try {
    String url = Constant.base_url + "get_movie_booking_detail";
    Constant.showLog('---show detail---');
    Constant.showLog(url);
    Map reqdata = {'booking_id': id};

    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog("show detail result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message':  'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}
