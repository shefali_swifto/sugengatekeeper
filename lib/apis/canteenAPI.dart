import 'dart:convert';

import 'package:sugengatekeeper/util/checkConnection.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:http/http.dart' as http;
import 'package:sugengatekeeper/util/keyConstant.dart';

Future<Map> getBooking(String bookingDt) async {
  try {
    String url = Constant.base_url + "get_item_wise_booking_list";
    Constant.showLog("===Get Booking List===");
    Constant.showLog(url);
    Map reqdata = {'booking_date': bookingDt};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
          headers: {"brand-id": Constant.brand_id}
      );
      Constant.showLog("get booking list Result");
      Constant.showLog(response.body);
      return {
        'success':1,
        'data':json.decode(response.body)
      };
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map> getEmpBooking(String bookingDt,String itemId) async {
  try {
    String url = Constant.base_url + "get_emp_details_for_item_wise_booking";
    Constant.showLog("===Get Emp Booking List===");
    Constant.showLog(url);
    Map reqdata = {'booking_date': bookingDt,'item_id':itemId};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
          headers: {"brand-id": Constant.brand_id}
      );
      Constant.showLog("get Emp booking list Result");
      Constant.showLog(response.body);
      return {
        'success':1,
        'data':json.decode(response.body)
      };
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}