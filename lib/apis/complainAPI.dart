import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sugengatekeeper/util/checkConnection.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:http/http.dart' as http;

Future<Map> getUserComplaintList(String type, BuildContext context) async {
  try {
    String user = await KeyConstant.retriveString(KeyConstant.key_user);
    Map userRes = json.decode(user);
    String url = Constant.base_url + "get_technician_complain_list";
    Constant.showLog('---Get UserComplaint list---');
    Constant.showLog(url);
    bool conn = await isConnection();
    Map reqdata = {'technician_id': userRes['id'].toString(), 'type': type};
    Constant.showLog(reqdata);
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog("Get Usercomplaint list");
      Constant.showLog(response.body);
      List list = json.decode(response.body);
      if (list == null) list = [];
      return {'success': 1, 'data': list};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map> getDetail(String cid, BuildContext context) async {
  try {
    String url = Constant.base_url + "get_complaint_detail";
    Constant.showLog('---Get Complaint Detail---');
    Constant.showLog(url);
    Map reqdata = {'complaint_id': cid};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog("Get complaint detail");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map> updateStatusComplaint(
    String cid, String status, List? imgs,String? cmt, BuildContext context) async {
  try {
    String url = Constant.base_url + "update_complaint";
    Constant.showLog('---update status Complaint---');
    Constant.showLog(url);
    Map reqdata = {
      'complaint_id': cid,
      'status': status,
      'role':'technician'
    };
    if (imgs != null)
      reqdata['complaint_solution_images'] = json.encode(imgs);
    if(cmt!=null)
      reqdata['technician_comment']=cmt;
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog("update status complaint");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}
