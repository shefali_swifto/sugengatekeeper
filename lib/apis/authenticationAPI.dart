import 'dart:convert';

import 'package:sugengatekeeper/util/checkConnection.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:http/http.dart' as http;
import 'package:sugengatekeeper/util/keyConstant.dart';

Future<Map> login(String usernm, String pwd) async {
  try {
    String url = Constant.base_url + "security_guard_login";
    Constant.showLog("===Login===");
    Constant.showLog(url);
    Map reqdata = {'username': usernm, 'password': pwd};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("login Phone Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> appointmentList(String dt) async {
  try {
    String url = Constant.base_url + "get_visitor_appointment_by_date";
    Constant.showLog(url);
    Map reqdata = {'appointment_date': dt};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog(response.body);
      return {'success': 1, 'data': json.decode(response.body)};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> getOnboardList() async {
  try {
    String url = Constant.base_url + "get_onboard_visitor_appointment";
    Constant.showLog(url);
    bool conn = await isConnection();
    if (conn) {
      final response = await http
          .post(Uri.parse(url), headers: {"brand-id": Constant.brand_id});
      return {'success': 1, 'data': json.decode(response.body)};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> appointmentDetail(String appId) async {
  try {
    String url = Constant.base_url + "get_visitor_appointment_detail";
    Constant.showLog(url);
    Map reqdata = {'appointment_id': appId};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> updateAppointmentDetail(String appId, String detailId,
    {String? vehicleNo,
    String? temp,
    String? present,
    String? sttm,
    String? endtm,
    String? isVacc,
    String? isMask}) async {
  try {
    String url = Constant.base_url + "update_visitor_appointment_detail";
    Constant.showLog(url);
    Map reqdata = {
      'appointment_id': appId,
      'appointment_detail_id': detailId,
    };
    if (vehicleNo != null) reqdata['vehicle_no'] = vehicleNo;
    if (temp != null) reqdata['temp'] = temp;
    if (present != null && present.isNotEmpty) reqdata['is_present'] = present;
    if (sttm != null) reqdata['start_time'] = sttm;
    if (endtm != null) reqdata['end_time'] = endtm;
    if (isVacc != null && isVacc.isNotEmpty) reqdata['is_vaccinated'] = isVacc;
    if (isMask != null && isMask.isNotEmpty) reqdata['is_mask'] = isMask;
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> updateAppointment(String appId, String status,
    {String? remark, String? outTime}) async {
  try {
    String url = Constant.base_url + "update_visitor_appointment";
    Constant.showLog(url);
    Map reqdata = {'appointment_id': appId, 'status': status};
    if (remark != null) reqdata['gatekeeper_remark'] = remark;
    if (outTime != null) reqdata['visitor_out_time'] = outTime;
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map?> editVisitor(String appId, String visitorId, String gender,
    String email, String? photo, String? idProof, String vehicle) async {
  try {
    String url = Constant.base_url + "update_visitor_data_in_appointment";
    Constant.showLog(url);
    Map reqdata = {'appointment_id': appId, 'visitor_id': visitorId};
    reqdata['gender'] = gender;
    reqdata['email'] = email;
    reqdata['photo'] = photo;
    reqdata['id_proof'] = idProof;
    reqdata['vehicle_no'] = vehicle;
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata, headers: {"brand-id": Constant.brand_id});
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}

Future<Map> updateFCM(String deviceid, String devicetype, String fcmid) async {
  try {
    String user = await KeyConstant.retriveString(KeyConstant.key_user);
    Map userRes=json.decode(user);
    String url = Constant.base_url + "torrent_register_device";
    Constant.showLog("===Update FCM===");
    Constant.showLog(url);

    Map reqdata = {
      'user_id':userRes['id'].toString(),
      'device_id': deviceid,
      'fcm_key': fcmid
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url),
          body: reqdata,
          headers: {"brand-id": Constant.brand_id}
      );
      Constant.showLog("update fcm result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': 'No Internet Connection'
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': 'Something wrong!!!Try Later'
    };
    return res;
  }
}
