import 'package:sugengatekeeper/dialog/alertDialog.dart';
import 'package:sugengatekeeper/loginScreen.dart';
import 'package:sugengatekeeper/model/app.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({Key? key}) : super(key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Constant.appName),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SwitchListTile(
              secondary: Icon(
                Icons.brightness_2,
                color: Theme.of(context).colorScheme.secondary,
                size: 24,
              ),
              value: Provider.of<AppModel>(context).darkTheme,
              activeColor: const Color(0xFF0066B4),
              onChanged: (bool value) {
                if (value) {
                  Provider.of<AppModel>(context, listen: false)
                      .updateTheme(true);
                } else {
                  Provider.of<AppModel>(context, listen: false)
                      .updateTheme(false);
                }
              },
              title: const Text(
                'Dark Theme',
              ),
            ),
            const Divider(height: 1,),
            ListTile(
              title: const Text("Logout"),
              leading: Icon(Icons.logout),
              onTap: ()async{
                await showDialog(context: context, builder: (BuildContext context){
                  return CustomAlertDialog(title: 'Logout Alert!!!', msg: 'You want to logout?', posClick: ()async{
                    Navigator.of(context).pop();
                    await KeyConstant.saveString(KeyConstant.key_user, '');
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return LoginScreen();
                          },
                        ),ModalRoute.withName('/')
                    );
                  },negClick: (){
                    Navigator.of(context).pop();
                  },);
                });

              },
            ),
            const Divider(height: 1,),
          ],
        ),
      ),
    );
  }
}
