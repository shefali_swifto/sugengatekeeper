import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:sugengatekeeper/util/styles.dart';

class CustomUI {
  static SnackBar showToast(String msg, BuildContext context) {
    return SnackBar(
      content: Text(
        msg,
      ),
    );
  }

  static showProgress(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                    Theme.of(context).colorScheme.secondary)),
          );
        });
  }

  static Widget CustProgress(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
          valueColor:
              AlwaysStoppedAnimation<Color>(Theme.of(context).colorScheme.secondary)),
    );
  }

  static hideProgress(BuildContext context) {
    Navigator.pop(context);
  }

  static customAlert(String title, String msg, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: EdgeInsets.all(10),
          contentPadding: EdgeInsets.all(10),
          title: Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: Text(msg),
          actions: <Widget>[
            TextButton(
              style: flatButtonStyle,
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK',style: TextStyle(color: btnTextClr),),
            )
          ],
        );
      },
    );
  }

  static customAlertDialog(String title, String msg, Function posClick,
      Function negClick, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: EdgeInsets.fromLTRB(20, 20, 20, 5),
          contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5),
          title: Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: Text(msg),
          actions: <Widget>[
            TextButton(
                style: flatButtonStyle,
                onPressed:()=>negClick,
                child: Text('No',style: TextStyle(color: btnTextClr),)),
            TextButton(
                style: flatButtonStyle,
                onPressed: ()=>posClick,
                child: Text('Yes',style: TextStyle(color: btnTextClr),)),
          ],
        );
      },
    );
  }
}

// No Internet Connection
class NoInternetDialog extends StatefulWidget {
  final Function? retryClick;
  NoInternetDialog({Key? key, this.retryClick});

  @override
  _NoInternetDialogState createState() => _NoInternetDialogState();
}

class _NoInternetDialogState extends State<NoInternetDialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Text('No Internet Connection'),
            SizedBox(
              height: 20,
            ),
            SwiftoBtn('Try Again', widget.retryClick)
          ],
        ),
      ),
    ));
  }
}
