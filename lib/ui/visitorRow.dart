import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/model/appointmentPodo.dart';
import 'package:sugengatekeeper/screens/visitor/visitorDetailScreen.dart';

class VisitorRow extends StatelessWidget {
  AppointmentPojo data;
  double fontSize = 16;
  VisitorRow({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime dt =
        DateFormat("yyyy-MM-dd HH:mm:ss").parse(data.appointmentDateTime!);
    DateTime? onboardtm;
    if (data.visitorOnboardTime != null && data.visitorOnboardTime!.isNotEmpty)
      onboardtm =
          new DateFormat("yyyy-MM-dd HH:mm:ss").parse(data.visitorOnboardTime!);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) {
                  return VistiorDetailScreen(
                    appointmentId: data.id.toString(),
                  );
                },
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                          'No of person:' + data.totalVisitor.toString(),
                          style: TextStyle(fontSize: fontSize)),
                      flex: 1,
                    ),
                    Text('Visitor Id:' + data.otp!,
                        style: TextStyle(fontSize: fontSize)),
                  ],
                ),
                const SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    Expanded(
                        child: Text(data.visitorCategoryName!,
                            style: TextStyle(fontSize: fontSize)),
                        flex: 1),
                    Expanded(
                      flex: 1,
                      child: Text('Type: ' + data.visitTypeName!,
                          style: TextStyle(fontSize: fontSize),textAlign: TextAlign.right),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 3,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                          DateFormat("dd MMM yyyy hh:mm aa").format(dt),
                          style: TextStyle(fontSize: fontSize),textAlign: TextAlign.start,
                      ),
                    ),
                    if(onboardtm!=null)
                    Expanded(
                      flex: 1,
                      child: Text(
                          'In Time: '+DateFormat("dd MMM yyyy hh:mm aa").format(onboardtm),
                          style: TextStyle(fontSize: fontSize),textAlign: TextAlign.right
                      ),
                    ),

                  ],
                ),
                const SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        data.visitorData!.name! + ' ' + data.visitorData!.phone!,
                        style: TextStyle(fontSize: fontSize),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5),
                      child: Text(data.status.toString().toUpperCase(),
                        style: TextStyle(
                          fontSize: fontSize,
                          fontWeight: FontWeight.bold,
                        ),textAlign: TextAlign.right,),
                    )
                  ],
                ),
                if(data.custData!=null && data.custData!.name!=null)
                  Container(
                    margin: EdgeInsets.only(top: 3),
                    child: Text(
                      'Invite By: '+data.custData!.name!,
                      style: TextStyle(fontSize: fontSize),
                      textAlign: TextAlign.right,
                    ),
                  ),
              ],
            ),
          ),
        ),
        const Divider(),
      ],
    );
  }
}
