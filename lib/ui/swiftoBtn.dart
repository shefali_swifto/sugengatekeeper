import 'package:sugengatekeeper/util/styles.dart';
import 'package:flutter/material.dart';

class SwiftoBtn extends StatelessWidget {
  String? btnText;
  Function? btnClick;
  Color? bgClr;

  SwiftoBtn(this.btnText,this.btnClick,{super.key, this.bgClr});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            backgroundColor:(bgClr!=null)?this.bgClr: defaultClr
        ),
      onPressed: ()=>btnClick!(),
      child:  Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width/2,
        child: Text(
          btnText!,
          style: const TextStyle(color: btnTextClr),
        ),
      )
    );
  }
}

class SwiftoBtn1 extends StatelessWidget {
  String? btnText;
  Function? btnClick;

  SwiftoBtn1(this.btnText,this.btnClick, {super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          backgroundColor: defaultClr
      ),
      onPressed:()=> btnClick!(),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
          btnText!,
          style: const TextStyle(color: btnTextClr),
        ),
      ),
    );
  }
}

class SwiftoRadio extends StatelessWidget {
  bool? isSelect;
  String? txt;
  VoidCallback? onChange;

  SwiftoRadio(
      {super.key,
        required this.isSelect,
        required this.txt,
        required this.onChange});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onChange,
      child: Row(
        children: [
          Icon(
            (isSelect!) ? Icons.radio_button_checked : Icons.radio_button_off,
            color: (isSelect!)
                ? Theme.of(context).colorScheme.secondary
                : Theme.of(context).iconTheme.color,
          ),
          const SizedBox(
            width: 3,
          ),
          Text(
            txt!,
          )
        ],
      ),
    );
  }
}
