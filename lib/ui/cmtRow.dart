import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/util/styles.dart';

class CommentRow extends StatelessWidget {
  Map c;
  String userRole;

  CommentRow(this.c,this.userRole);

  @override
  Widget build(BuildContext context) {
    DateFormat defaultfmt = new DateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dtfmt = new DateFormat("dd MMM yy hh:mm aa");
    Color? clr;
    String st = c['status'].toString();
    if (st.toLowerCase() == "acknowledge") {
      clr = acknowledgeClr;
    } else if (st.toLowerCase() == "pending") {
      clr = pendingClr;
    } else if (st.toLowerCase() == "solved") {
      clr = solvedClr;
    }else if (st.toLowerCase() == "complete") {
      clr = completeClr;
    } else if (st.toLowerCase() == "cancel") {
      clr = cancelClr;
    }else if (st.toLowerCase() == "closed") {
      clr = closedClr;
    }
    DateTime dt1 = defaultfmt.parse(c['updated_at']);
    String tm = dtfmt.format(dt1);
    bool isOwner=false;
    if(c['added_by'].toString().toLowerCase()==userRole)
      isOwner=true;
    String nm=c['added_by'].toString().toUpperCase();
    if(nm.toLowerCase()=='customer')
      nm='Complaint By';
    if(nm.contains("_"))
      nm=nm.replaceAll("_"," ");
    return Container(
      alignment: (!isOwner)?Alignment.centerLeft:Alignment.centerRight,
      child: Card(
        color: Theme.of(context).cardColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)),
        child: Container(
          width: MediaQuery.of(context).size.width/1.5,
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if(!isOwner)
                Text(nm),
              if(!isOwner)
                SizedBox(height: 3,),
              Text(c['comment'].toString()),
              SizedBox(height: 3,),
              Text(c['status'].toString().toUpperCase(),style: TextStyle(color: clr),),
              Container(alignment:Alignment.centerRight,child: Text(tm))
            ],
          ),
        ),
      ),
    );
  }
}
