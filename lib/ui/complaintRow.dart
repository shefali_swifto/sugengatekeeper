import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sugengatekeeper/util/styles.dart';

class ComplaintRow extends StatelessWidget {
  Map c;
  Function onRefresh, onClick;
  bool? showCustomer=false;

  ComplaintRow(this.c, this.onRefresh, this.onClick,{super.key, this.showCustomer=false});

  @override
  Widget build(BuildContext context) {
    DateFormat defaultfmt = new DateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dtfmt = new DateFormat("dd MMM yy hh:mm aa");
    DateTime dt = defaultfmt.parse(c['created_at']);
    String otime = dtfmt.format(dt),
        st = c['status'].toString(),
        title = c['cat_name'],loc='';
    int  id = c['id'];
    Color? clr;
    if (st.toLowerCase() == "acknowledge") {
      clr = acknowledgeClr;
    } else if (st.toLowerCase() == "pending") {
      clr = pendingClr;
    } else if (st.toLowerCase() == "solved") {
      clr = solvedClr;
    }else if (st.toLowerCase() == "complete") {
      clr = completeClr;
    } else if (st.toLowerCase() == "cancel") {
      clr = cancelClr;
    }else if (st.toLowerCase() == "closed") {
      clr = closedClr;
    }
    if (c.containsKey('sub_cat_name') && c['sub_cat_name'].toString().isNotEmpty) {
      title = title + ' (' + c['sub_cat_name'] + ')';
    }
    if (c.containsKey('location')) {
      if (c['location'] == 'house') {
        if(c['house_data']!=null && c['house_data']['house_no']!=null)
          loc = 'HouseNo '+c['house_data']['house_no'];
        else
          loc = 'HouseNo';
      } else {
        loc = 'Generic / Common';
      }
    }
    return Container(
      child: InkWell(
        onTap: () => onClick(),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(15.0, 10, 15, 10),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Complaint No. : $id\n'+'$title',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    if(showCustomer!)
                      Container(
                        margin: EdgeInsets.only(bottom: 2),
                        child: Text('Complaint By: '+c['customer_name']+" "+c['mobile_no'].toString()),
                      ),
                    if (c.containsKey('location'))
                      Container(
                          margin: EdgeInsets.only(bottom: 5),
                          child: Text('Location: ' + loc)),
                    Text(c['complain_text']),
                    SizedBox(
                      height: 2,
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            c['status'].toString().toUpperCase(),
                            style: TextStyle(color: clr),
                          ),
                          Text(otime),
                        ]),

                  ]),
            ),
            Divider()
          ],
        ),
      ),
    );
  }
}
