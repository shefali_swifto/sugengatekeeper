import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SwiftoTxtField extends StatelessWidget {
  TextEditingController _controller;
  String? hintTxt, prefixTxt;
  TextInputAction txtAction;
  TextInputType txtInputType;
  bool isMultiline;
  int? maxLen;

  SwiftoTxtField(this._controller, this.hintTxt, this.txtAction,
      this.txtInputType, this.isMultiline, this.maxLen, this.prefixTxt,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: TextField(
          keyboardType: txtInputType,
          textInputAction: txtAction,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.secondary)),
              prefixText: (prefixTxt != null) ? prefixTxt! + " " : prefixTxt,
              labelText: hintTxt,
              labelStyle: TextStyle(color: Theme.of(context).hintColor),
              counterText: ""),
          controller: _controller,
          minLines: 1,
          maxLines: (isMultiline) ? 5 : 1,
          maxLength: maxLen,
        ));
  }
}

class SwiftoValidTxtField extends StatelessWidget {
  Function? saveVal, validFun, onClick;
  String hintTxt, val;
  TextInputAction txtAction;
  TextInputType txtInputType;
  bool isMultiline = false, isReadOnly = false, isPwd = false;
  TextEditingController ctrl = new TextEditingController();

  SwiftoValidTxtField(this.val, this.hintTxt, this.txtAction, this.txtInputType,
      this.saveVal, this.validFun,
      {Key? key,
      this.onClick,
      this.isMultiline = false,
      this.isReadOnly = false,
      this.isPwd = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ctrl.text = val;
    return Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: TextFormField(
          onTap: () {
            if (onClick != null) {
              onClick!();
            }
          },
          onSaved: (input) =>
              (saveVal != null) ? saveVal!(input.toString()) : null,
          validator: (input) => (validFun != null) ? validFun!(input) : null,
          keyboardType: txtInputType,
          textInputAction: txtAction,
          readOnly: isReadOnly,
          obscureText: isPwd,
          controller: ctrl,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.secondary)),
              labelText: hintTxt,
              hintText: hintTxt,
              labelStyle: TextStyle(color: Theme.of(context).hintColor),
              counterText: "",
              suffixIcon:
                  (onClick != null) ? const Icon(Icons.arrow_drop_down) : null),
          minLines: (isMultiline) ? 3 : 1,
          maxLines: (isMultiline) ? 7 : 1,
        ));
  }
}
