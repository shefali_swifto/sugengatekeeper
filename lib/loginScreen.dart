import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:sugengatekeeper/apis/authenticationAPI.dart';
import 'package:sugengatekeeper/screens/canteen/bookingList.dart';
import 'package:sugengatekeeper/screens/complain/userComplaintScreen.dart';
import 'package:sugengatekeeper/screens/movietheater/bookingListScreen.dart';
import 'package:sugengatekeeper/screens/verifyOTPScreen.dart';
import 'package:sugengatekeeper/ui/swiftoBtn.dart';
import 'package:sugengatekeeper/ui/swiftoCustom.dart';
import 'package:sugengatekeeper/ui/swiftoTxtField.dart';
import 'package:sugengatekeeper/util/constant.dart';
import 'package:sugengatekeeper/util/keyConstant.dart';
import 'package:sugengatekeeper/screens/visitor/visitorListScreen.dart';
import 'package:flutter/material.dart';
import 'package:sugengatekeeper/screens/visitor/visitorScreen.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String username = '', pwd = '';

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  void setLoginData(Map res)async{
    Map user = res['user_data'];
    user['role_text'] = res['role'];
    await KeyConstant.saveString(KeyConstant.key_user, json.encode(user));
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (BuildContext context) {
            return (res['role'] == KeyConstant.role_gatekeeper)
                ? const VisitorScreen()
                : (res['role'] == KeyConstant.role_technician)
                ? const UserComplaintScreen()
                : (res['role'] == KeyConstant.role_show_checker)
                ? const BookingListScreen()
                : (res['role']==KeyConstant.role_canteen_admin)
                ? const BookingList()
                : Container();
          },
        ), ModalRoute.withName('/'));
  }

  Future sendOTP(Map res)async{
    if(res['user_data']['phone_no']!=null && res['user_data']['phone_no'].toString().isNotEmpty) {
      String? phoneNumber = '+91' + res['user_data']['phone_no'];
      CustomUI.showProgress(context);
      PhoneVerificationCompleted verificationCompleted =
          (PhoneAuthCredential phoneAuthCredential) async {
        Constant.showLog("==PhoneVerification Completed==");
        UserCredential userCredential = await _auth.signInWithCredential(
            phoneAuthCredential);
        setLoginData(res);
      };

      PhoneVerificationFailed verificationFailed =
          (FirebaseAuthException authException) {
        Constant.showLog("==PhoneVerificationFailed==");
        Constant.showLog(
            'Phone number verification failed. Code: ${authException
                .code}. Message: ${authException.message}');
        Navigator.pop(context);
        //checkUser(phoneNumber);
      };

      PhoneCodeSent codeSent =
          (String verificationId, [int? forceResendingToken]) async {
        //_verificationId = verificationId;
        Constant.showLog("==Sent==");
        Constant.showLog('sms code sent ' + verificationId);
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  VerifyCode(
                    fromCart: false,
                    verId: verificationId,
                    phoneNumber: phoneNumber!,
                    res: res,)),
        );
      };

      PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
          (String verificationId) {
        // _verificationId = verificationId;
        Constant.showLog("==PhoneCodeAutoRetrievalTimeout==");
      };

      try {
        await _auth.verifyPhoneNumber(
            phoneNumber: phoneNumber!,
            timeout: const Duration(seconds: 5),
            verificationCompleted: verificationCompleted,
            verificationFailed: verificationFailed,
            codeSent: codeSent,
            codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
      } catch (e) {
        Constant.showLog('Failed to Verify Phone Number: $e');
      }
    }else{
      setLoginData(res);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: const Text('Login'),
          elevation: 0.0),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Center(
            child: Form(
              key: formKey,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Center(
                      child: Container(
                    child: Image.asset(
                      "assets/logo.png",
                      height: 200.0,
                      width: 200.0,
                    ),
                  )),
                  const SizedBox(
                    height: 20.0,
                  ),
                  SwiftoValidTxtField(
                      username,
                      "UserName",
                      TextInputAction.next,
                      TextInputType.text,
                      (input) => username = input, (input) {
                    if (input.toString().isEmpty) {
                      return 'UserName required';
                    } else {
                      return null;
                    }
                  }),
                  SwiftoValidTxtField(pwd, "Password", TextInputAction.done,
                      TextInputType.text, (input) => pwd = input, (input) {
                    if (input.toString().isEmpty) {
                      return 'Password required';
                    } else {
                      return null;
                    }
                  }, isPwd: true),
                  Center(
                    child: SwiftoBtn('Sign In', () async {
                      if (formKey.currentState!.validate()) {
                        formKey.currentState!.save();
                        CustomUI.showProgress(context);
                        Map? res = await login(username, pwd);
                        CustomUI.hideProgress(context);
                        if (res != null && res.containsKey('success')) {
                          if (res['success'] == 1) {
                            sendOTP(res);
                          } else if (res.containsKey('message')) {
                            showToast(res['message']);
                          }
                        }
                      }
                    }),
                  ),
                  const SizedBox(
                    height: 25.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
